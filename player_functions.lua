-- CONSTANTS *************************************************************************************
RadioMenusAdded = {}

-- ADD RADIO MENUS ***
function AddPlayerRadioMenus()
    local BluePlayerClients = SET_CLIENT:New():FilterCoalitions("blue"):FilterActive(true):FilterOnce()
    BluePlayerClients:ForEachClient(
            function(Client)
                local GroupId = Client:GetClientGroupID()
                if RadioMenusAdded[tostring(GroupId)] == nil then
                    RadioMenusAdded[tostring(GroupId)] = true

                    local MissionStatusMenu = missionCommands.addSubMenuForGroup(GroupId, "Mission")
                    missionCommands.addCommandForGroup(GroupId, "Current Time", MissionStatusMenu, GetCurrentTime, GroupId)
                    missionCommands.addCommandForGroup(GroupId, "Orders", MissionStatusMenu, GetOrdersBlue, GroupId)
                    missionCommands.addCommandForGroup(GroupId, "Situation Report", MissionStatusMenu, GetSituationReport, GroupId)

                    local CapRequestMenu = missionCommands.addSubMenuForGroup(GroupId, "CAP Request")
                    missionCommands.addCommandForGroup(GroupId, "Bad Hollow", CapRequestMenu, RequestCAP, "Zone Bad Hollow", GroupId)
                    missionCommands.addCommandForGroup(GroupId, "Sunshine Valley", CapRequestMenu, RequestCAP, "Zone Sunshine Valley", GroupId)

                    local CasRequestMenu = missionCommands.addSubMenuForGroup(GroupId, "CAS Tasking")
                    missionCommands.addCommandForGroup(GroupId, "Bad Hollow", CasRequestMenu, RequestCasBlue, "Zone Bad Hollow", GroupId)
                    missionCommands.addCommandForGroup(GroupId, "Sunshine Valley", CasRequestMenu, RequestCasBlue, "Zone Sunshine Valley", GroupId)
                end
            end
    )
    local RedPlayerClients = SET_CLIENT:New():FilterCoalitions("red"):FilterActive(true):FilterOnce()
    RedPlayerClients:ForEachClient(
            function(Client)
                local GroupId = Client:GetClientGroupID()
                if RadioMenusAdded[tostring(GroupId)] == nil then
                    RadioMenusAdded[tostring(GroupId)] = true

                    local MissionStatusMenu = missionCommands.addSubMenuForGroup(GroupId, "Mission")
                    missionCommands.addCommandForGroup(GroupId, "Current Time", MissionStatusMenu, GetCurrentTime, GroupId)
                    missionCommands.addCommandForGroup(GroupId, "Orders", MissionStatusMenu, GetOrdersRed, GroupId)
                    missionCommands.addCommandForGroup(GroupId, "Situation Report", MissionStatusMenu, GetSituationReport, GroupId)

                    local CapRequestMenu = missionCommands.addSubMenuForGroup(GroupId, "CAP Request")
                    missionCommands.addCommandForGroup(GroupId, "Bad Hollow", CapRequestMenu, RequestCAPRed, "Zone Bad Hollow", GroupId)
                    missionCommands.addCommandForGroup(GroupId, "Sunshine Valley", CapRequestMenu, RequestCAPRed, "Zone Sunshine Valley", GroupId)

                    local CasRequestMenu = missionCommands.addSubMenuForGroup(GroupId, "CAS Tasking")
                    missionCommands.addCommandForGroup(GroupId, "Bad Hollow", CasRequestMenu, RequestCasRed, "Zone Bad Hollow", GroupId)
                    missionCommands.addCommandForGroup(GroupId, "Sunshine Valley", CasRequestMenu, RequestCasRed, "Zone Sunshine Valley", GroupId)
                end
            end
    )
end

function GetCurrentTime(GroupId)
    local CurrentTime = UTILS.TimeNow()
    trigger.action.outTextForGroup(GroupId, string.format("Mission Time: %s", CurrentTime), 30, false)
    trigger.action.outSoundForGroup(GroupId, "walkietalkie.ogg")
end

function GetOrdersBlue(GroupId)
    local OrdersMessage
    if IsMigOrSamDay == "MiG" then
        OrdersMessage = [[
        Sitrep:
        SAM activity occasionally detected.
        MiG activity is expected.

        Orders:
        Provide fighter cover and intercept
        in enemy airspace if required.
        CAS flights engage in any zone as required.
        SEAD flights are free to engage any live SAM.
        ]]
    elseif IsMigOrSamDay == "SAM" then
        OrdersMessage = [[
        Sitrep:
        SAM activity detected.
        MiG activity is expected.

        Orders:
        Provide fighter cover and avoid enemy airspace.
        CAS flights provide support except Alfa zone.
        SEAD flights are free to engage any live SAM.
        ]]
    end
    trigger.action.outTextForGroup(GroupId, OrdersMessage, 30, false)
    trigger.action.outSoundForGroup(GroupId, "walkietalkie.ogg")
end

function GetOrdersRed(GroupId)
    local OrdersMessage
    if IsMigOrSamDay == "MiG" then
        OrdersMessage = [[
        Sitrep:
        SAMs are online and switch off when our squadrons
        are in air to avoid friendly fire incidents.

        Orders:
        Engage any enemy air units in our airspace.
        ]]
    elseif IsMigOrSamDay == "SAM" then
        OrdersMessage = [[
        Sitrep:
        SAMs are active and all squadrons are operational in coordination.
        SAMs will not switch off.

        Orders:
        Engage any enemy air units in our airspace.
        ]]
    end
    trigger.action.outTextForGroup(GroupId, OrdersMessage, 30, false)
    trigger.action.outSoundForGroup(GroupId, "walkietalkie.ogg")
end

function GetSituationReport(GroupId)
    local SituationReport = REPORT:New("Situation Report")
    SituationReport:Add("## North ##")
    SituationReport:Add(" ")
    SituationReport:Add("Zulu is " .. ZoneZulu_Ops:GetOwnerName())
    SituationReport:Add("Yankee is " .. ZoneYankee_Ops:GetOwnerName())
    SituationReport:Add("XRay is " .. ZoneXRay_Ops:GetOwnerName())
    SituationReport:Add("Whiskey is " .. ZoneWhiskey_Ops:GetOwnerName())
    SituationReport:Add("Taftanaz airbase is " .. ZoneTaftanaz_Ops:GetOwnerName())
    SituationReport:Add(" ")
    SituationReport:Add("## South ##")
    SituationReport:Add("Alfa is " .. ZoneAlfa_Ops:GetOwnerName())
    SituationReport:Add("Beta is " .. ZoneBeta_Ops:GetOwnerName())
    SituationReport:Add("Charlie is " .. ZoneCharlie_Ops:GetOwnerName())
    SituationReport:Add("Delta is " .. ZoneDelta_Ops:GetOwnerName())
    SituationReport:Add("Rayak is " .. ZoneRayak_Ops:GetOwnerName())

    trigger.action.outTextForGroup(GroupId, SituationReport:Text(), 30, false)
    trigger.action.outSoundForGroup(GroupId, "walkietalkie.ogg")
end

function RequestCAP(ZoneName, GroupId)
    local CAPZone = ZONE:FindByName(ZoneName)
    local CAPMission = AUFTRAG:NewGCICAP(CAPZone:GetCoordinate(), 15000, 285, 90, 20)
    if ZoneName == "Zone Bad Hollow" then
        BlueChiefNorth:AddMission(CAPMission)
    elseif ZoneName == "Zone Sunshine Valley" then
        BlueChiefSouth:AddMission(CAPMission)
    else
        env.info("OPL this should not happen")
    end

    trigger.action.outTextForGroup(GroupId, string.format("CAP enroute to %s", ZoneName), 30, false)
    trigger.action.outSoundForGroup(GroupId, "walkietalkie.ogg")
end

function RequestCAPRed(ZoneName, GroupId)
    local CAPZone = ZONE:FindByName(ZoneName)
    local CAPMission = AUFTRAG:NewGCICAP(CAPZone:GetCoordinate(), math.random(3000, 1000), 300, math.random(0, 359), math.random(10, 20))
    if ZoneName == "Zone Bad Hollow" then
        RedChiefNorth:AddMission(CAPMission)
    elseif ZoneName == "Zone Sunshine Valley" then
        RedChiefSouth:AddMission(CAPMission)
    else
        env.info("OPL this should not happen")
    end

    trigger.action.outTextForGroup(GroupId, string.format("CAP enroute to %s", ZoneName), 30, false)
    trigger.action.outSoundForGroup(GroupId, "walkietalkie.ogg")
end

function RequestCasBlue(ZoneName, GroupId)
    local CASZone = ZONE:FindByName(ZoneName)
    CASZone:Scan({ Object.Category.UNIT }, { Unit.Category.GROUND_UNIT })
    local GroupSetInZone = CASZone:GetScannedSetGroup()
    local HighestThreatGroup
    local HighestThreatGroupThreatLevel = 0
    GroupSetInZone:ForEachGroup(
            function(groupInZone)
                env.info("OPL scanning zone task " .. groupInZone:GetName())
                if groupInZone:GetCoalition() == coalition.side.RED then
                    if groupInZone:CalculateThreatLevelA2G() > HighestThreatGroupThreatLevel then
                        HighestThreatGroupThreatLevel = groupInZone:CalculateThreatLevelA2G()
                        HighestThreatGroup = groupInZone
                    end
                end
            end
    )
    if HighestThreatGroup == nil then
        trigger.action.outTextForGroup(GroupId, "No targets", 30, false)
        trigger.action.outSoundForGroup(GroupId, "walkietalkie.ogg")
        return
    else
        local MarkCoordinate = HighestThreatGroup:GetCoordinate()
        local MapMark = MarkCoordinate:MarkToCoalitionBlue(HighestThreatGroup:GetName(), false)
        trigger.action.outTextForGroup(GroupId, string.format("Target %s marked on map", HighestThreatGroup:GetName()), 30, false)
        trigger.action.outSoundForGroup(GroupId, "walkietalkie.ogg")
    end
end

function RequestCasRed(ZoneName, GroupId)
    local CASZone = ZONE:FindByName(ZoneName)
    CASZone:Scan({ Object.Category.UNIT }, { Unit.Category.GROUND_UNIT })
    local GroupSetInZone = CASZone:GetScannedSetGroup()
    local HighestThreatGroup
    local HighestThreatGroupThreatLevel = 0
    GroupSetInZone:ForEachGroup(
            function(groupInZone)
                env.info("OPL scanning zone task " .. groupInZone:GetName())
                if groupInZone:GetCoalition() == coalition.side.BLUE then
                    if groupInZone:CalculateThreatLevelA2G() > HighestThreatGroupThreatLevel then
                        HighestThreatGroupThreatLevel = groupInZone:CalculateThreatLevelA2G()
                        HighestThreatGroup = groupInZone
                    end
                end
            end
    )
    if HighestThreatGroup == nil then
        trigger.action.outTextForGroup(GroupId, "No targets", 30, false)
        trigger.action.outSoundForGroup(GroupId, "walkietalkie.ogg")
        return
    else
        local MarkCoordinate = HighestThreatGroup:GetCoordinate()
        local MapMark = MarkCoordinate:MarkToCoalitionRed(HighestThreatGroup:GetName(), false)
        trigger.action.outTextForGroup(GroupId, string.format("Target %s marked on map", HighestThreatGroup:GetName()), 30, false)
        trigger.action.outSoundForGroup(GroupId, "walkietalkie.ogg")
    end
end

-- reschedule for users that change slot or coalition
local RadioMenuScheduler = TIMER:New(AddPlayerRadioMenus)
RadioMenuScheduler:Start(5, 30)

ZZZLoaderAssert["6-player-functions"] = true