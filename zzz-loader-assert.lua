for missionScriptName, isMissionScriptLoaded in pairs(ZZZLoaderAssert) do
    if isMissionScriptLoaded then
        local logMessage = string.format("%s loaded OK", missionScriptName)
        trigger.action.outText(logMessage, 10)
        env.info(logMessage)
    else
        local logMessage = string.format("%s loaded ERROR", missionScriptName)
        trigger.action.outText(logMessage, 30)
        trigger.action.outSound("alert.ogg")
        env.error(logMessage)
    end
end