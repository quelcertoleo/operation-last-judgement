-- TODO someday we will have a decent FAC in DCS, for now this is just dead code
--[[
local FACMissionSunshineValley = AUFTRAG:NewFAC(ZONE_POLYGON:FindByName("Zone Sunshine Valley"), 90, 15000, 133.25, 0)
BlueChief:AddMission(FACMissionSunshineValley)
local FACMissionBadHollow = AUFTRAG:NewFAC(ZONE_POLYGON:FindByName("Zone Bad Hollow"), 90, 15000, 133.35, 0)
BlueChief:AddMission(FACMissionBadHollow)
]]

-- some days will be mig days, other days will be sam days, very rarely it will be a sam and mig day
if IsMigOrSamDay == "MiG" then

    RedMantis = MANTIS:New(
            "RedMantis",
            "Red SAM",
            "Red EWR",
            "Red HQ", -- red hq group name
            "red",
            false, -- is not dynamic filter, new units will not be added to mantis
            nil,
            true) --will switch emissions off
    RedMantis:SetSAMRange(math.random(50, 75))
    RedMantis:Debug(false)
    RedMantis.verbose = false -- watch DCS.log
    RedMantis.checkforfriendlies = true
    RedMantis:SetUsingEmOnOff(true)
    RedMantis:Start()

    env.info("OPL RedChief has a MiG day")
elseif IsMigOrSamDay == "SAM" then

    RedMantis = MANTIS:New(
            "RedMantis",
            "Red SAM",
            "Red EWR",
            "Red HQ", -- red hq group name
            "red",
            false, -- is not dynamic filter, new units will not be added to mantis
            nil,
            true) --will switch emissions off
    RedMantis:SetSAMRange(math.random(50, 75))
    RedMantis:Debug(false)
    RedMantis.verbose = false -- watch DCS.log
    RedMantis.checkforfriendlies = false
    RedMantis:SetUsingEmOnOff(true)
    RedMantis:Start()

    env.info("OPL RedChief has a MiG and SAM day")
end

-- at night RedChief stops everything, SAMs always on so BlueChief will be cautious
if UTILS.TimeLaterThan("18:00:00") or UTILS.TimeBefore("06:00:00") then
    -- stop all ground offensive
    AirwingDuhur:Stop()
    AirwingSayqal:Stop()
    AirwingShayrat:Stop()
    AirwingMezzeh:Stop()
    AirwingTaftanaz:Stop()
    RedBrigadeNorth:Stop()
    RedBrigadeSouth:Stop()

    RedMantis = MANTIS:New(
            "RedMantis",
            "Red SAM",
            "Red EWR",
            "Red HQ", -- red hq group name
            "red",
            false, -- is not dynamic filter, new units will not be added to mantis
            nil,
            true) --will switch emissions off
    RedMantis:SetSAMRange(math.random(50, 75))
    RedMantis:Debug(false)
    RedMantis.verbose = false -- watch DCS.log
    RedMantis.checkforfriendlies = false
    RedMantis:SetUsingEmOnOff(true)
    RedMantis:Start()
end

ZZZLoaderAssert["5-hardcoded-missions"] = true