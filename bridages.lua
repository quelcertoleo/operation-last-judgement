--[[
NOTE
in hardcoded_missions.lua all units with name "Platoon" in group name that are not in a safe zone will be removed
if you have groups with Platoon name in Mission Editor, they will be removed and crash this file
Leave Platoon only in the PlatoonName field on :NEW() to be parsed at mission start
]]
-- blue south
-- troops
BluePlatoonSouth = PLATOON:New("Blue GI", 32, "Blue GI Platoon South")
BluePlatoonSouth:SetAttribute(GROUP.Attribute.GROUND_INFANTRY)
BluePlatoonSouth:AddMissionCapability({
    AUFTRAG.Type.ONGUARD,
    AUFTRAG.Type.CAPTUREZONE })
BluePlatoonSouth:AddWeaponRange(0, 2)

BlueTroopsBrigadeSouth = BRIGADE:New("Blue Warehouse Troops South", "Blue Troops Brigade South")
BlueTroopsBrigadeSouth:SetSpawnZone(ZONE:FindByName("FARP London Spawn Zone"))
BlueTroopsBrigadeSouth:AddPlatoon(BluePlatoonSouth)
BlueTroopsBrigadeSouth:Start()

-- ground units
BluePlatoonArmouredSouth = PLATOON:New("Blue Armour South", 8, "Blue Armoured Platoon South")
BluePlatoonArmouredSouth:SetAttribute(GROUP.Attribute.GROUND_TANK)
BluePlatoonArmouredSouth:AddMissionCapability({
    AUFTRAG.Type.ARMOREDGUARD,
    AUFTRAG.Type.PATROLZONE })
BluePlatoonArmouredSouth:AddWeaponRange(0, 2)

BlueBrigadeSouth = BRIGADE:New("Blue Warehouse South", "Blue Brigade South")
BlueBrigadeSouth:SetSpawnZone(GetBlueChiefSouthFrontlineSpawn(), UTILS.NMToMeters(100))
BlueBrigadeSouth:AddPlatoon(BluePlatoonArmouredSouth)
BlueBrigadeSouth:Start()

-- blue north
-- tropps
BluePlatoonNorth = PLATOON:New("Blue GI", 32, "Blue GI Platoon North")
BluePlatoonNorth:SetAttribute(GROUP.Attribute.GROUND_INFANTRY)
BluePlatoonNorth:AddMissionCapability({
    AUFTRAG.Type.ONGUARD,
    AUFTRAG.Type.CAPTUREZONE })
BluePlatoonNorth:AddWeaponRange(0, 2)

BlueTroopsBrigadeNorth = BRIGADE:New("Blue Warehouse Troops North", "Blue Troops Brigade North")
BlueTroopsBrigadeNorth:SetSpawnZone(ZONE:FindByName("FARP Dallas Spawn Zone"))
BlueTroopsBrigadeNorth:AddPlatoon(BluePlatoonNorth)
BlueTroopsBrigadeNorth:Start()

-- ground units
BluePlatoonArmouredNorth = PLATOON:New("Blue Armour North", 8, "Blue Armoured Platoon North")
BluePlatoonArmouredNorth:SetAttribute(GROUP.Attribute.GROUND_TANK)
BluePlatoonArmouredNorth:AddMissionCapability({
    AUFTRAG.Type.ONGUARD,
    AUFTRAG.Type.ARMOREDGUARD,
    AUFTRAG.Type.CAPTUREZONE,
    AUFTRAG.Type.PATROLZONE })
BluePlatoonArmouredNorth:AddWeaponRange(0, 2)

BlueBrigadeNorth = BRIGADE:New("Blue Warehouse Bad Hollow-1", "Blue Brigade North")
BlueBrigadeNorth:SetSpawnZone(GetBlueChiefNorthFrontlineSpawn(), UTILS.NMToMeters(100))
BlueBrigadeNorth:AddPlatoon(BluePlatoonArmouredNorth)
BlueBrigadeNorth:Start()

-- red south
--troops
RedPlatoonSouth = PLATOON:New("Red GI", 16, "Red GI Platoon South")
RedPlatoonSouth:SetAttribute(GROUP.Attribute.GROUND_INFANTRY)
RedPlatoonSouth:AddMissionCapability({
    AUFTRAG.Type.ONGUARD,
    AUFTRAG.Type.CAPTUREZONE })
RedPlatoonSouth:AddWeaponRange(0, 2)

RedTroopsBrigadeSouth = BRIGADE:New("Red Warehouse Troops South", "Red Troops Brigade South")
RedTroopsBrigadeSouth:SetSpawnZone(ZONE:FindByName("Mezzeh Spawn Zone"))
RedTroopsBrigadeSouth:AddPlatoon(RedPlatoonSouth)
RedTroopsBrigadeSouth:Start()

--ground units
RedPlatoonArmoured = PLATOON:New("Red Armour", 8, "Red Armoured Platoon-1")
RedPlatoonArmoured:SetAttribute(GROUP.Attribute.GROUND_TANK)
RedPlatoonArmoured:AddMissionCapability({
    AUFTRAG.Type.ARMOREDGUARD,
    AUFTRAG.Type.PATROLZONE })
RedPlatoonArmoured:AddWeaponRange(0, 2)

RedPlatoonToyota = PLATOON:New("Red Toyota", 8, "Red Armoured Platoon-2")
RedPlatoonToyota:SetAttribute(GROUP.Attribute.GROUND_TANK)
RedPlatoonToyota:AddMissionCapability({
    AUFTRAG.Type.ARMOREDGUARD,
    AUFTRAG.Type.PATROLZONE })
RedPlatoonToyota:AddWeaponRange(0, 2)

RedArtyTemplate = PLATOON:New("Red Arty XCL", 8, "Red Arty Platoon XCL")
RedArtyTemplate:SetAttribute(GROUP.Attribute.GROUND_ARTILLERY)
RedArtyTemplate:AddMissionCapability({
    AUFTRAG.Type.ARTY })
RedArtyTemplate:AddWeaponRange(0, 2.5)

RedArtyRocketTemplate = PLATOON:New("Red Arty Rocket XCL", 8, "Red Arty Rocket Platoon XCL")
RedArtyRocketTemplate:SetAttribute(GROUP.Attribute.GROUND_ARTILLERY)
RedArtyRocketTemplate:AddMissionCapability({
    AUFTRAG.Type.ARTY })
RedArtyRocketTemplate:AddWeaponRange(0, 7)

RedBrigadeSouth = BRIGADE:New("Red Warehouse South-1", "Red Brigade South")
RedBrigadeSouth:SetSpawnZone(GetRedChiefSouthFrontlineSpawnZone(), UTILS.NMToMeters(100))
RedBrigadeSouth:AddPlatoon(RedPlatoonArmoured)
RedBrigadeSouth:AddPlatoon(RedPlatoonToyota)
RedBrigadeSouth:AddPlatoon(RedArtyTemplate)
RedBrigadeSouth:AddPlatoon(RedArtyRocketTemplate)
RedBrigadeSouth:Start()

-- red north
-- troops
RedPlatoonNorth = PLATOON:New("Red GI", 16, "Red GI Platoon North")
RedPlatoonNorth:SetAttribute(GROUP.Attribute.GROUND_INFANTRY)
RedPlatoonNorth:AddMissionCapability({
    AUFTRAG.Type.ONGUARD,
    AUFTRAG.Type.CAPTUREZONE })
RedPlatoonNorth:AddWeaponRange(0, 2)

RedTroopsBrigadeNorth = BRIGADE:New("Red Warehouse Troops North", "Red Troops Brigade North")
RedTroopsBrigadeNorth:SetSpawnZone(ZONE:FindByName("Taftanaz Spawn Zone"))
RedTroopsBrigadeNorth:AddPlatoon(RedPlatoonNorth)
RedTroopsBrigadeNorth:Start()

-- ground units
RedPlatoonArmouredNorth = PLATOON:New("Red Armour", 4, "Red Armoured Platoon North")
RedPlatoonArmouredNorth:SetAttribute(GROUP.Attribute.GROUND_TANK)
RedPlatoonArmouredNorth:AddMissionCapability({
    AUFTRAG.Type.ARMOREDGUARD,
    AUFTRAG.Type.PATROLZONE })
RedPlatoonArmouredNorth:AddWeaponRange(0, 2)

RedPlatoonToyotaNorth = PLATOON:New("Red Toyota", 8, "Red Toyota Platoon North")
RedPlatoonToyotaNorth:SetAttribute(GROUP.Attribute.GROUND_TANK)
RedPlatoonToyotaNorth:AddMissionCapability({
    AUFTRAG.Type.ARMOREDGUARD,
    AUFTRAG.Type.PATROLZONE })
RedPlatoonToyotaNorth:AddWeaponRange(0, 2)

RedArtyTemplateNorth = PLATOON:New("Red Arty XCL", 8, "Red Arty Platoon XCL North")
RedArtyTemplateNorth:SetAttribute(GROUP.Attribute.GROUND_ARTILLERY)
RedArtyTemplateNorth:AddMissionCapability({
    AUFTRAG.Type.ARTY })
RedArtyTemplateNorth:AddWeaponRange(0, 2.5)

RedBrigadeNorth = BRIGADE:New("Red Warehouse Bad Hollow-1", "Red Brigade North")
RedBrigadeNorth:SetSpawnZone(GetRedChiefNorthFrontlineSpawnZone(), UTILS.NMToMeters(100))
RedBrigadeNorth:AddPlatoon(RedPlatoonArmouredNorth)
RedBrigadeNorth:AddPlatoon(RedPlatoonToyotaNorth)
RedBrigadeNorth:Start()

ZZZLoaderAssert["3-brigades"] = true