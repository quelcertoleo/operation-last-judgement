--blue
local BluePlayerTaskGround = PLAYERTASKCONTROLLER:New("Blue Tasking", coalition.side.BLUE, PLAYERTASKCONTROLLER.Type.A2GS)
BluePlayerTaskGround:SetLocale("en")
BluePlayerTaskGround:SetupIntel({ "Blue" })
BluePlayerTaskGround:SetMenuName("Tasking")
BluePlayerTaskGround:EnableMarkerOps("Tasking")
BluePlayerTaskGround:SetMarkerReadOnly()
BluePlayerTaskGround:SetEnableSmokeFlareTask()
BluePlayerTaskGround:SetEnableUseTypeNames()
BluePlayerTaskGround:SetDisableIlluminateTask()
--BluePlayerTaskGround:AddRejectZone(ZONE_POLYGON:FindByName("Red Border"))
BluePlayerTaskGround:SetTargetRadius(2000)

function BluePlayerTaskGround:OnAfterTaskAdded(From, Event, To, Task)
    local TaskAddedMessage = string.format("Task %s added", Task:GetTarget():GetName())
    env.info("OPL BluePlayerTaskGround:OnAfterTaskAdded -> " .. TaskAddedMessage)
    trigger.action.outTextForCoalition(coalition.side.BLUE, TaskAddedMessage, 15)
    trigger.action.outSoundForCoalition(coalition.side.BLUE, "walkietalkie2.ogg")
end
function BluePlayerTaskGround:OnAfterTaskProgress(From, Event, To, Task, TargetCount)
    local TaskProgressMessage = string.format("Target destroyed, %d remaining", TargetCount)
    env.info("OPL BluePlayerTaskGround:OnAfterTaskProgress -> " .. TaskProgressMessage)
    trigger.action.outTextForCoalition(coalition.side.BLUE, TaskProgressMessage, 15)
    trigger.action.outSoundForCoalition(coalition.side.BLUE, "walkietalkie2.ogg")
end
function BluePlayerTaskGround:OnAfterTaskSuccess(From, Event, To, Task)
    local TaskSuccessMessage = string.format("Task %s completed", Task:GetTarget():GetName())
    env.info("OPL BluePlayerTaskGround:OnAfterTaskSuccess -> " .. TaskSuccessMessage)
    trigger.action.outTextForCoalition(coalition.side.BLUE, TaskSuccessMessage, 15)
    trigger.action.outSoundForCoalition(coalition.side.BLUE, "walkietalkie2.ogg")
end

-- red
local RedPlayerTaskGround = PLAYERTASKCONTROLLER:New("Red Tasking", coalition.side.RED, PLAYERTASKCONTROLLER.Type.A2GS)
RedPlayerTaskGround:SetLocale("en")
RedPlayerTaskGround:SetupIntel({ "Red" })
RedPlayerTaskGround:SetMenuName("Tasking")
RedPlayerTaskGround:EnableMarkerOps("Tasking")
RedPlayerTaskGround:SetMarkerReadOnly()
RedPlayerTaskGround:SetEnableSmokeFlareTask()
RedPlayerTaskGround:SetEnableUseTypeNames()
RedPlayerTaskGround:SetDisableIlluminateTask()
--BluePlayerTaskGround:AddRejectZone(ZONE_POLYGON:FindByName("Red Border"))
RedPlayerTaskGround:SetTargetRadius(2000)

function RedPlayerTaskGround:OnAfterTaskAdded(From, Event, To, Task)
    local TaskAddedMessage = string.format("Task %s added", Task:GetTarget():GetName())
    env.info("OPL BluePlayerTaskGround:OnAfterTaskAdded -> " .. TaskAddedMessage)
    trigger.action.outTextForCoalition(coalition.side.RED, TaskAddedMessage, 15)
    trigger.action.outSoundForCoalition(coalition.side.RED, "walkietalkie2.ogg")
end
function RedPlayerTaskGround:OnAfterTaskProgress(From, Event, To, Task, TargetCount)
    local TaskProgressMessage = string.format("Target destroyed, %d remaining", TargetCount)
    env.info("OPL BluePlayerTaskGround:OnAfterTaskProgress -> " .. TaskProgressMessage)
    trigger.action.outTextForCoalition(coalition.side.RED, TaskProgressMessage, 15)
    trigger.action.outSoundForCoalition(coalition.side.RED, "walkietalkie2.ogg")
end
function RedPlayerTaskGround:OnAfterTaskSuccess(From, Event, To, Task)
    local TaskSuccessMessage = string.format("Task %s completed", Task:GetTarget():GetName())
    env.info("OPL BluePlayerTaskGround:OnAfterTaskSuccess -> " .. TaskSuccessMessage)
    trigger.action.outTextForCoalition(coalition.side.RED, TaskSuccessMessage, 15)
    trigger.action.outSoundForCoalition(coalition.side.RED, "walkietalkie2.ogg")
end

ZZZLoaderAssert["7-player-tasking"] = true