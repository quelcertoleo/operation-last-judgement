ZZZLoaderAssert = {
    ["1-last-judgement"] = false,
    ["2-airwings"] = false,
    ["3-brigades"] = false,
    ["4-chiefs"] = false,
    ["5-hardcoded-missions"] = false,
    ["6-player-functions"] = false,
    ["7-player-tasking"] = false
}

_SETTINGS:SetPlayerMenuOff()
_SETTINGS:SetEraModern()
_SETTINGS:SetA2A_BULLS()
_SETTINGS:SetA2G_LL_DDM()
_SETTINGS:SetImperial()

-- CTLD
BluePlayerHeliGroupNames = { "FARP London UH-1H", "FARP Dallas UH-1H" }
local CtldBlue = CTLD:New(coalition.side.BLUE, BluePlayerHeliGroupNames, "BLUE Logistics")
CtldBlue.SmokeColor = SMOKECOLOR.Green -- default color to use when dropping smoke from heli
CtldBlue.FlareColor = FLARECOLOR.Green -- color to use when flaring from heli
CtldBlue.RadioSound = "beacon.ogg" -- -- this sound will be hearable if you tune in the beacon frequency. Add the sound file to your miz.
CtldBlue.RadioSoundFC3 = "beaconsilent.ogg"
CtldBlue.dropcratesanywhere = true
CtldBlue.cratecountry = country.id.USA
CtldBlue:__Start(5)
CtldBlue:AddCTLDZone("CTLD Blue South", CTLD.CargoZoneType.LOAD, SMOKECOLOR.Green, true, true)
CtldBlue:AddCTLDZone("CTLD Blue North", CTLD.CargoZoneType.LOAD, SMOKECOLOR.Green, true, true)
CtldBlue:AddTroopsCargo("Infantry Team", { "Blue CTLD GI" }, CTLD_CARGO.Enum.TROOPS, 4, 100)
CtldBlue:AddTroopsCargo("RPG Team", { "Blue CTLD RPG" }, CTLD_CARGO.Enum.TROOPS, 4, 100)
CtldBlue:AddTroopsCargo("Mortar Team", { "Blue CTLD Mortar" }, CTLD_CARGO.Enum.TROOPS, 4, 100)
CtldBlue:AddTroopsCargo("Stinger Team", { "Blue CTLD Stinger" }, CTLD_CARGO.Enum.TROOPS, 2, 100, nil)
CtldBlue:AddCratesCargo("Field AAA", { "Blue CTLD AAA" }, CTLD_CARGO.Enum.VEHICLE, 1, 250, nil)

RedPlayerHeliGroupNames = { "Mezzeh Mi-24", "Mezzeh Mi-8", "Taftanaz Mi-8", "Taftanaz Mi-24" }
local CtldRed = CTLD:New(coalition.side.RED, RedPlayerHeliGroupNames, "RED Logistics")
CtldRed.SmokeColor = SMOKECOLOR.Red -- default color to use when dropping smoke from heli
CtldRed.FlareColor = FLARECOLOR.Red -- color to use when flaring from heli
CtldRed.RadioSound = "beacon.ogg" -- -- this sound will be hearable if you tune in the beacon frequency. Add the sound file to your miz.
CtldRed.RadioSoundFC3 = "beaconsilent.ogg"
CtldRed.dropcratesanywhere = true
CtldRed.cratecountry = country.id.RUSSIA
CtldRed:__Start(5)
CtldRed:AddCTLDZone("CTLD Red Mezzeh", CTLD.CargoZoneType.LOAD, SMOKECOLOR.Red, true, true)
CtldRed:AddCTLDZone("CTLD Red Taftanaz", CTLD.CargoZoneType.LOAD, SMOKECOLOR.Red, true, true)
CtldRed:AddTroopsCargo("Infantry Team", { "Red CTLD GI" }, CTLD_CARGO.Enum.TROOPS, 4, 100)
CtldRed:AddTroopsCargo("RPG Team", { "Red CTLD RPG" }, CTLD_CARGO.Enum.TROOPS, 4, 100)
CtldRed:AddTroopsCargo("Mortar Team", { "Red CTLD Mortar" }, CTLD_CARGO.Enum.TROOPS, 4, 100)
CtldRed:AddTroopsCargo("Stinger Team", { "Red CTLD Strela" }, CTLD_CARGO.Enum.TROOPS, 2, 100, nil)
CtldRed:AddCratesCargo("Field AAA", { "Red CTLD AAA" }, CTLD_CARGO.Enum.VEHICLE, 1, 250, nil)

-- zones
ZoneAlfa = ZONE_POLYGON:NewFromDrawing("Zone Capture Alfa")
ZoneAlfa_Ops = OPSZONE:New(ZoneAlfa, coalition.side.NEUTRAL):SetMarkZone(true, true)
ZoneAlfa_Ops:SetUnitCategories({ Unit.Category.GROUND_UNIT })
ZoneAlfa_Ops:SetCaptureThreatlevel(3)
ZoneAlfa_Ops:SetCaptureNunits(2)
ZoneAlfa_Ops:Start()
ZoneAlfaSpawn = ZONE:FindByName("Zone Alfa Spawn")

ZoneBeta = ZONE_POLYGON:NewFromDrawing("Zone Capture Beta")
ZoneBeta_Ops = OPSZONE:New(ZoneBeta, coalition.side.NEUTRAL):SetMarkZone(true, true)
ZoneBeta_Ops:SetUnitCategories({ Unit.Category.GROUND_UNIT })
ZoneBeta_Ops:SetCaptureThreatlevel(3)
ZoneBeta_Ops:SetCaptureNunits(2)
ZoneBeta_Ops:Start()
ZoneBetaSpawn = ZONE:FindByName("Zone Beta Spawn")

ZoneCharlie = ZONE_POLYGON:NewFromDrawing("Zone Capture Charlie")
ZoneCharlie_Ops = OPSZONE:New(ZoneCharlie, coalition.side.NEUTRAL):SetMarkZone(true, true)
ZoneCharlie_Ops:SetUnitCategories({ Unit.Category.GROUND_UNIT })
ZoneCharlie_Ops:SetCaptureThreatlevel(3)
ZoneCharlie_Ops:SetCaptureNunits(2)
ZoneCharlie_Ops:Start()
ZoneCharlieSpawn = ZONE:FindByName("Zone Charlie Spawn")

ZoneDelta = ZONE_POLYGON:NewFromDrawing("Zone Capture Delta")
ZoneDelta_Ops = OPSZONE:New(ZoneDelta, coalition.side.NEUTRAL):SetMarkZone(true, true)
ZoneDelta_Ops:SetUnitCategories({ Unit.Category.GROUND_UNIT })
ZoneDelta_Ops:SetCaptureThreatlevel(3)
ZoneDelta_Ops:SetCaptureNunits(2)
ZoneDelta_Ops:Start()
ZoneDeltaSpawn = ZONE:FindByName("Zone Delta Spawn")

ZoneRayak = ZONE_POLYGON:NewFromDrawing("Zone Capture Rayak")
ZoneRayak_Ops = OPSZONE:New(ZoneRayak, coalition.side.NEUTRAL):SetMarkZone(true, true)
ZoneRayak_Ops:SetUnitCategories({ Unit.Category.GROUND_UNIT })
ZoneRayak_Ops:SetCaptureThreatlevel(3)
ZoneRayak_Ops:SetCaptureNunits(2)
ZoneRayak_Ops:Start()
ZoneRayakSpawn = ZONE:FindByName("Zone Rayak Spawn")

ZoneZulu = ZONE_POLYGON:NewFromDrawing("Zone Capture Zulu")
ZoneZulu_Ops = OPSZONE:New(ZoneZulu, coalition.side.NEUTRAL):SetMarkZone(true, true)
ZoneZulu_Ops:SetUnitCategories({ Unit.Category.GROUND_UNIT })
ZoneZulu_Ops:SetCaptureThreatlevel(3)
ZoneZulu_Ops:SetCaptureNunits(2)
ZoneZulu_Ops:Start()
ZoneZuluSpawn = ZONE:FindByName("Zone Zulu Spawn")

ZoneYankee = ZONE_POLYGON:NewFromDrawing("Zone Capture Yankee")
ZoneYankee_Ops = OPSZONE:New(ZoneYankee, coalition.side.NEUTRAL):SetMarkZone(true, true)
ZoneYankee_Ops:SetUnitCategories({ Unit.Category.GROUND_UNIT })
ZoneYankee_Ops:SetCaptureThreatlevel(3)
ZoneYankee_Ops:SetCaptureNunits(2)
ZoneYankee_Ops:Start()
ZoneYankeeSpawn = ZONE:FindByName("Zone Yankee Spawn")

ZoneXRay = ZONE_POLYGON:NewFromDrawing("Zone Capture XRay")
ZoneXRay_Ops = OPSZONE:New(ZoneXRay, coalition.side.NEUTRAL):SetMarkZone(true, true)
ZoneXRay_Ops:SetUnitCategories({ Unit.Category.GROUND_UNIT })
ZoneXRay_Ops:SetCaptureThreatlevel(3)
ZoneXRay_Ops:SetCaptureNunits(2)
ZoneXRay_Ops:Start()
ZoneXRaySpawn = ZONE:FindByName("Zone XRay Spawn")

ZoneWhiskey = ZONE_POLYGON:NewFromDrawing("Zone Capture Whiskey")
ZoneWhiskey_Ops = OPSZONE:New(ZoneWhiskey, coalition.side.NEUTRAL):SetMarkZone(true, true)
ZoneWhiskey_Ops:SetUnitCategories({ Unit.Category.GROUND_UNIT })
ZoneWhiskey_Ops:SetCaptureThreatlevel(3)
ZoneWhiskey_Ops:SetCaptureNunits(2)
ZoneWhiskey_Ops:Start()
ZoneWhiskeySpawn = ZONE:FindByName("Zone Whiskey Spawn")

ZoneTaftanaz = ZONE_AIRBASE:New(AIRBASE.Syria.Taftanaz, 1000)
ZoneTaftanaz_Ops = OPSZONE:New(ZoneTaftanaz, coalition.side.NEUTRAL):SetMarkZone(true, true)
ZoneTaftanaz_Ops:SetUnitCategories({ Unit.Category.GROUND_UNIT })
ZoneTaftanaz_Ops:SetCaptureThreatlevel(3)
ZoneTaftanaz_Ops:SetCaptureNunits(2)
ZoneTaftanaz_Ops:Start()

-- MANTIS ***
BlueMantis = MANTIS:New(
        "BlueMantis",
        "Blue SAM",
        "Blue EWR", -- no EWR prefix for blue
        "Blue HQ", -- blue hq group name
        "blue",
        false, -- is not dynamic filter, new units will not be added to mantis
        nil,
        false) -- will not switch emissions off
BlueMantis:SetSAMRange(75)
BlueMantis:Debug(false)
BlueMantis.verbose = false -- watch DCS.log
BlueMantis:Start()

-- used when zone is CAPTURED, we will spawn in the same zone that was captured
function GetBlueChiefSouthFrontlineSpawn()
    if ZoneAlfa_Ops:IsBlue() then
        return ZoneAlfaSpawn
    elseif ZoneBeta_Ops:IsBlue() then
        return ZoneBetaSpawn
    elseif ZoneCharlie_Ops:IsBlue() then
        return ZoneCharlieSpawn
    elseif ZoneRayak_Ops:IsBlue() then
        return ZoneRayakSpawn
    elseif ZoneDelta_Ops:IsBlue() then
        return ZoneDeltaSpawn
    else
        return ZONE:FindByName("Blue Zone Spawn South")
    end
end

-- used when zone is CAPTURED, we will spawn in the same zone that was captured
function GetBlueChiefSouthPreviousSpawnZone()
    if ZoneAlfa_Ops:IsBlue() then
        return ZoneBetaSpawn
    elseif ZoneBeta_Ops:IsBlue() then
        return ZoneCharlieSpawn
    elseif ZoneCharlie_Ops:IsBlue() then
        return ZoneRayakSpawn
    elseif ZoneRayak_Ops:IsBlue() then
        return ZoneDeltaSpawn
    else
        return ZONE:FindByName("Blue Zone Spawn South")
    end
end

-- used when zone is CAPTURED, we will spawn in the same zone that was captured
function GetRedChiefSouthFrontlineSpawnZone()
    if ZoneDelta_Ops:IsRed() then
        return ZoneDeltaSpawn
    elseif ZoneRayak_Ops:IsRed() then
        return ZoneRayakSpawn
    elseif ZoneCharlie_Ops:IsRed() then
        return ZoneCharlieSpawn
    elseif ZoneBeta_Ops:IsRed() then
        return ZoneBetaSpawn
    elseif ZoneAlfa_Ops:IsRed() then
        return ZoneAlfaSpawn
    else
        return ZONE:FindByName("Red Zone Spawn South")
    end
end

-- used when zone is LOST, we spawn on the previous area, not in the same we just lost
function GetRedChiefSouthPreviousSpawnZone()
    if ZoneDelta_Ops:IsRed() then
        return ZoneRayakSpawn
    elseif ZoneRayak_Ops:IsRed() then
        return ZoneCharlieSpawn
    elseif ZoneCharlie_Ops:IsRed() then
        return ZoneBetaSpawn
    elseif ZoneBeta_Ops:IsRed() then
        return ZoneAlfaSpawn
    elseif ZoneAlfa_Ops:IsRed() then
        return ZONE:FindByName("Red Zone Spawn South")
    else
        return ZONE:FindByName("Red Zone Spawn South")
    end
end

-- used when zone is CAPTURED, we will spawn in the same zone that was captured
function GetBlueChiefNorthFrontlineSpawn()
    if ZoneTaftanaz_Ops:IsBlue() then
        return ZoneWhiskey
    elseif ZoneWhiskey_Ops:IsBlue() then
        return ZoneWhiskey
    elseif ZoneXRay_Ops:IsBlue() then
        return ZoneXRaySpawn
    elseif ZoneYankee_Ops:IsBlue() then
        return ZoneYankeeSpawn
    elseif ZoneZulu_Ops:IsBlue() then
        return ZoneZuluSpawn
    else
        return ZONE:FindByName("Blue Zone Spawn Bad Hollow")
    end
end

-- used when zone is LOST, we spawn on the previous area, not in the same we just lost
function GetBlueChiefNorthPreviousSpawnZone()
    if ZoneTaftanaz_Ops:IsBlue() then
        return ZoneWhiskey
    elseif ZoneWhiskey_Ops:IsBlue() then
        return ZoneXRay_Ops
    elseif ZoneXRay_Ops:IsBlue() then
        return ZoneYankeeSpawn
    elseif ZoneYankee_Ops:IsBlue() then
        return ZoneZuluSpawn
    else
        return ZONE:FindByName("Blue Zone Spawn Bad Hollow")
    end
end

-- used when zone is CAPTURED, we will spawn in the same zone that was captured
function GetRedChiefNorthFrontlineSpawnZone()
    if ZoneZulu_Ops:IsRed() then
        return ZoneZuluSpawn
    elseif ZoneYankee_Ops:IsRed() then
        return ZoneYankeeSpawn
    elseif ZoneXRay_Ops:IsRed() then
        return ZoneXRaySpawn
    elseif ZoneWhiskey_Ops:IsRed() then
        return ZoneWhiskeySpawn
    else
        return ZONE:FindByName("Red Zone Spawn Bad Hollow")
    end
end

-- used when zone is LOST, we spawn on the previous area, not in the same we just lost
function GetRedChiefNorthPreviousSpawnZone()
    if ZoneZulu_Ops:IsRed() then
        return ZoneYankeeSpawn
    elseif ZoneYankee_Ops:IsRed() then
        return ZoneXRaySpawn
    elseif ZoneXRay_Ops:IsRed() then
        return ZoneWhiskeySpawn
    elseif ZoneWhiskey_Ops:IsRed() then
        return ZONE:FindByName("Red Zone Spawn Bad Hollow")
    else
        return ZONE:FindByName("Red Zone Spawn Bad Hollow")
    end
end

-- remove "in transit" units, other units with XCL in group name won't be saved by DSMC, remember to configure DSMC accordingly
local PlatoonGroups = GROUP:FindAllByMatching("Platoon")
local SafeZones = {
    ZoneAlfa,
    ZoneBeta,
    ZoneCharlie,
    ZoneDelta,
    ZoneRayak,
    ZoneZulu,
    ZoneYankee,
    ZoneXRay,
    ZoneTaftanaz }
for key, platoon in pairs(PlatoonGroups) do
    local isPlatoonSafe = false
    for _, zone in pairs(SafeZones) do
        env.info("OPL Checking if platoon " .. platoon:GetName() .. " is in zone " .. zone:GetName())
        if platoon:IsInZone(zone) then
            isPlatoonSafe = true
        end
    end
    if not isPlatoonSafe then
        env.info(string.format("OPL Platoon %s is not in a OpsZone at mission start, will be removed", platoon:GetName()))
        platoon:Destroy()
    end
end

-- is MiG, SAM or Both day
IsMigOrSamDay = nil
local DayRandom = math.random(0, 10)
if DayRandom <= 6 then
    IsMigOrSamDay = "MiG"
else
    IsMigOrSamDay = "SAM"
end

ZZZLoaderAssert["1-last-judgement"] = true