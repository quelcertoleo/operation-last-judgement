# Operation Last Judgement - 15th July 1974

Redfor is seeking to expand its own borders and is aggressively pushing towards two areas codenamed Sunshine Valley and Bad Hollow.

Blufor objective is to nullify Redfor military capacity.

Both coalition objective is to capture all key areas in the map.

Inspiration by Mole Cricket 19, Toyota War, 1982 Lebanon War

## Map and Operation Theater

![screen1](./screenies/screen1.png)
![screen1](./screenies/screen2.png)
![screen1](./screenies/screen3.png)
![screen1](./screenies/screen4.png)
![screen1](./screenies/screen5.png)

## Description

### Supported Units
* A-4E-C
* AJS37 Viggen
* F-4E Phantom
* F-5E Tiger
* Mi-8
* Mi-24
* MiG-19
* MiG-21
* Mirage F1
* Su-25 FC3
* UH-1H

### Required downloads

* Unmarked liveries for AJS37, Mirage F-1CE/EE https://www.digitalcombatsimulator.com/en/files/3334314/
* Unmarked liveries for AJS37, F-5E, MiG-19, MiG-21 https://www.digitalcombatsimulator.com/en/files/3334260/
* Unmarked liveries for F-4E (desert package) https://www.digitalcombatsimulator.com/en/files/3338320/
* Unmarked liveries for F-4E (SEA package) https://www.digitalcombatsimulator.com/en/files/3338319/
* Mig-19P Egyptian Airforce, https://www.digitalcombatsimulator.com/en/files/3317357/
* High Digit Sams (don't worry SA-2, SA-3, SA-5 only) for more realistic SA-2 and SA-3, https://github.com/Auranis/HighDigitSAMs
* A-4E Community Project, https://heclak.github.io/community-a4e-c/
* (optional) DSMC to save missions state and persistence https://dsmcfordcs.wordpress.com/

### DSMC Setup Recommendations
* Enable "Keep destroyed vehicles"
* Use continuous time for saved scenery
* Configure XCL to be used as exclusion pattern for saving (some units are not required to be saved between sessions)

## How it works

Mission will start with Redfor moving forward to Bad Hollow.
On Sunshine Valley there will be a race for the capture zones and the front will eventually stabilize at the center of the valley.
Redfor and Blufor have a limited amount of air assets, every loss they have will be counted for and not available on next mission.

After 19:00:00 Redfor will stop all air and ground operations with the exception of intercepting incursions in their airspace.

Some days will be "MiG Days" and some other days will be "SAM Days". When MiGs are flying there will be reduced Redfor SAM activity, to avoid friendly fire incidents.
Viceversa, when it will be a "SAM Day" MiGs will most likely stay on the ground.
This is not a hardline and things might change suddenly.

On "SAM Days", Bluefor will be more cautious and avoid the areas around Homs and Damascus on ground and air offensives.

Finally, if a Redfor airwing suffers excessive casualties it will backoff and activate nearby SAMs.

## How to use this mission / intended use cases

This mission is intended to be used PVP or PVE coop both by single or in multiplayer.
The intended use case is to start the mission, have some fun for about 1-2 hours, save state and come back to it at a later moment.
It is not intended to be running on a server 24/7 as some events are decided at mission start but will not change during the mission, e.g. if it is a "MiG Day" or "SAM Day" is decided at mission start and will not change during the mission.
Additionally, airwing resources are limited and after launching a given amount of missions there will be no more AI airplanes.

## Feature Set

### Tasking

Bluefor and Redfor have a dynamic tasking system that will create tasks for players depending on battlefield situation. Ground support operations will dynamically create F10 map marks.

F10 Map marks will be created in locations where enemy units have been detected.

### Bigeye EWR

The mission includes Bigeye EWR (https://gitlab.com/quelcertoleo/big-eye-ewr)

### Splash Damage

The mission includes a revised version of Splash Damage script

## Customization

### Adding client units

There are no naming requirements to add units to Blue or Red, just avoid adding multiple client units in the same group because DCS has a limitation regarding F10 menu configuration that will result in double entries for groups with multiple clients.

### Modifying AI templates, units, SAMs

Be extremely careful and be sure to understand how the scripts work before embarking on editing. I will not explain the implementation details here but will simply summarize that unit names do matter, especially regarding the detection system, the persistence system and some events on the battlefield.