function GetRandomSkill()
    local RandomSkill = math.random(0, 100)
    if RandomSkill <= 50 then
        return AI.Skill.AVERAGE
    elseif RandomSkill > 50 and RandomSkill <= 85 then
        return AI.Skill.GOOD
    elseif RandomSkill > 85 then
        return AI.Skill.HIGH
    end
end

-- incirlik
Squadron_A4E_Incirlik = SQUADRON:New("A4E Template", math.random(2, 4), "Incirlik A4E")
Squadron_A4E_Incirlik:SetGrouping(2)
Squadron_A4E_Incirlik:SetFuelLowThreshold(30)
Squadron_A4E_Incirlik:SetFuelLowRefuel(false)
Squadron_A4E_Incirlik:SetTurnoverTime(math.random(60, 120), math.random(30, 60))
Squadron_A4E_Incirlik:SetModex(30)
Squadron_A4E_Incirlik:SetRadio(311)
Squadron_A4E_Incirlik:SetSkill(AI.Skill.HIGH)
Squadron_A4E_Incirlik:SetCallsign(CALLSIGN.Aircraft.Springfield, 4)
Squadron_A4E_Incirlik:SetMissionRange(150)
Squadron_A4E_Incirlik:AddMissionCapability({
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.BAI,
    AUFTRAG.Type.CAS,
    AUFTRAG.Type.CASENHANCED,
    AUFTRAG.Type.SEAD })

Squadron_F4E_Incirlik = SQUADRON:New("F4E Template", math.random(2, 4), "Incirlik F4E")
Squadron_F4E_Incirlik:SetGrouping(2)
Squadron_F4E_Incirlik:SetDespawnAfterLanding()
Squadron_F4E_Incirlik:SetFuelLowThreshold(40)
Squadron_F4E_Incirlik:SetFuelLowRefuel(false)
Squadron_F4E_Incirlik:SetTurnoverTime(math.random(60, 120), math.random(30, 60))
Squadron_F4E_Incirlik:SetModex(220)
Squadron_F4E_Incirlik:SetRadio(311)
Squadron_F4E_Incirlik:SetSkill(AI.Skill.HIGH)
Squadron_F4E_Incirlik:SetCallsign(CALLSIGN.Aircraft.Springfield, 5)
Squadron_F4E_Incirlik:SetMissionRange(150)
Squadron_F4E_Incirlik:AddMissionCapability({
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.GCICAP,
    AUFTRAG.Type.CAP,
    AUFTRAG.Type.INTERCEPT,
    AUFTRAG.Type.ESCORT,
    AUFTRAG.Type.BAI,
    AUFTRAG.Type.GROUNDATTACK,
    AUFTRAG.Type.SEAD })

Squadron_F5E_Incirlik = SQUADRON:New("F5E Template", math.random(2, 4), "Incirlik F5E")
Squadron_F5E_Incirlik:SetGrouping(2)
Squadron_F5E_Incirlik:SetDespawnAfterLanding()
Squadron_F5E_Incirlik:SetFuelLowThreshold(40)
Squadron_F5E_Incirlik:SetFuelLowRefuel(false)
Squadron_F5E_Incirlik:SetTurnoverTime(math.random(60, 120), math.random(30, 60))
Squadron_F5E_Incirlik:SetModex(220)
Squadron_F5E_Incirlik:SetRadio(311)
Squadron_F5E_Incirlik:SetSkill(AI.Skill.HIGH)
Squadron_F5E_Incirlik:SetCallsign(CALLSIGN.Aircraft.Springfield, 6)
Squadron_F5E_Incirlik:SetMissionRange(120)
Squadron_F5E_Incirlik:AddMissionCapability({
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.INTERCEPT,
    AUFTRAG.Type.BAI,
    AUFTRAG.Type.GROUNDATTACK })

Squadron_AJS37_Incirlik = SQUADRON:New("AJS37 Template", math.random(2, 4), "Incirlik AJS37")
Squadron_AJS37_Incirlik:SetGrouping(2)
Squadron_AJS37_Incirlik:SetDespawnAfterLanding()
Squadron_AJS37_Incirlik:SetFuelLowThreshold(40)
Squadron_AJS37_Incirlik:SetFuelLowRefuel(false)
Squadron_AJS37_Incirlik:SetTurnoverTime(math.random(60, 120), math.random(30, 60))
Squadron_AJS37_Incirlik:SetModex(30)
Squadron_AJS37_Incirlik:SetRadio(311)
Squadron_AJS37_Incirlik:SetSkill(AI.Skill.HIGH)
Squadron_AJS37_Incirlik:SetCallsign(CALLSIGN.Aircraft.Springfield, 7)
Squadron_AJS37_Incirlik:SetMissionRange(120)
Squadron_AJS37_Incirlik:AddMissionCapability({
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.BAI,
    AUFTRAG.Type.GROUNDATTACK })

AirwingIncirlik = AIRWING:New("Warehouse Incirlik-1", "Incirlik Airwing")
AirwingIncirlik:SetAirbase(AIRBASE:FindByName(AIRBASE.Syria.Incirlik))
AirwingIncirlik:SetTakeoffHot()
AirwingIncirlik:AddSquadron(Squadron_F4E_Incirlik)
AirwingIncirlik:NewPayload("F4E CAP", -1, {
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.GCICAP,
    AUFTRAG.Type.CAP,
    AUFTRAG.Type.INTERCEPT,
    AUFTRAG.Type.ESCORT })
AirwingIncirlik:NewPayload("F4E BAI", -1, {
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.BAI })
AirwingIncirlik:NewPayload("F4E SEAD", -1, {
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.SEAD })
AirwingIncirlik:AddSquadron(Squadron_F5E_Incirlik)
AirwingIncirlik:NewPayload("F5E CAP", -1, {
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.INTERCEPT })
AirwingIncirlik:NewPayload("F5E BAI", -1, {
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.BAI })
AirwingIncirlik:AddSquadron(Squadron_AJS37_Incirlik)
AirwingIncirlik:NewPayload("AJS37 Template", -1, {
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.BAI,
    AUFTRAG.Type.GROUNDATTACK })
AirwingIncirlik:AddSquadron(Squadron_A4E_Incirlik)
AirwingIncirlik:NewPayload("A4E CAS", -1, {
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.BAI,
    AUFTRAG.Type.CAS,
    AUFTRAG.Type.CASENHANCED })
AirwingIncirlik:NewPayload("A4E SEAD", -1, {
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.SEAD })

function AirwingIncirlik:onafterFlightOnMission(From, Event, To, FlightGroup, Mission)
    local MissionStartMessage = string.format("%s  mission %s", FlightGroup:GetName(), Mission:GetType())
    trigger.action.outTextForCoalition(coalition.side.BLUE, MissionStartMessage, 15)
    trigger.action.outSoundForCoalition(coalition.side.BLUE, "walkietalkie2.ogg")
    env.info("OPL BlueChief" .. MissionStartMessage)
end

-- ramat
Blue_Squadron_E2 = SQUADRON:New("E2 Hawkeye", 2, "E2 Hawkeye Squadron")
Blue_Squadron_E2:SetGrouping(1)
Blue_Squadron_E2:SetModex(501)
Blue_Squadron_E2:SetRadio(235.5)
Blue_Squadron_E2:SetSkill(AI.Skill.EXCELLENT)
Blue_Squadron_E2:SetCallsign(CALLSIGN.AWACS.Overlord)
Blue_Squadron_E2:AddMissionCapability({
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.AWACS })

Squadron_A4E_Ramat = SQUADRON:New("A4E Template Ramat", math.random(2, 4), "Ramat A4E")
Squadron_A4E_Ramat:SetGrouping(2)
Squadron_A4E_Ramat:SetFuelLowThreshold(30)
Squadron_A4E_Ramat:SetFuelLowRefuel(false)
Squadron_A4E_Ramat:SetTurnoverTime(math.random(60, 120), math.random(30, 60))
Squadron_A4E_Ramat:SetModex(320)
Squadron_A4E_Ramat:SetRadio(313)
Squadron_A4E_Ramat:SetSkill(AI.Skill.HIGH)
Squadron_A4E_Ramat:SetCallsign(CALLSIGN.Aircraft.Colt, 5)
Squadron_A4E_Ramat:SetMissionRange(150)
Squadron_A4E_Ramat:AddMissionCapability({
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.BAI,
    AUFTRAG.Type.CAS,
    AUFTRAG.Type.CASENHANCED })

Squadron_F4E_Ramat = SQUADRON:New("F4E Template", math.random(2, 4), "Ramat F4E")
Squadron_F4E_Ramat:SetGrouping(2)
Squadron_F4E_Ramat:SetDespawnAfterLanding()
Squadron_F4E_Ramat:SetFuelLowThreshold(40)
Squadron_F4E_Ramat:SetFuelLowRefuel(false)
Squadron_F4E_Ramat:SetTurnoverTime(math.random(60, 120), math.random(30, 60))
Squadron_F4E_Ramat:SetModex(250)
Squadron_F4E_Ramat:SetRadio(313)
Squadron_F4E_Ramat:SetSkill(AI.Skill.HIGH)
Squadron_F4E_Ramat:SetCallsign(CALLSIGN.Aircraft.Colt, 6)
Squadron_F4E_Ramat:SetMissionRange(150)
Squadron_F4E_Ramat:AddMissionCapability({
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.GCICAP,
    AUFTRAG.Type.CAP,
    AUFTRAG.Type.INTERCEPT,
    AUFTRAG.Type.ESCORT,
    AUFTRAG.Type.BAI,
    AUFTRAG.Type.GROUNDATTACK,
    AUFTRAG.Type.SEAD })

Squadron_F5E_Ramat = SQUADRON:New("F5E Template Ramat", math.random(2, 4), "Ramat F5E")
Squadron_F5E_Ramat:SetGrouping(2)
Squadron_F5E_Ramat:SetDespawnAfterLanding()
Squadron_F5E_Ramat:SetFuelLowThreshold(40)
Squadron_F5E_Ramat:SetFuelLowRefuel(false)
Squadron_F5E_Ramat:SetTurnoverTime(math.random(60, 120), math.random(30, 60))
Squadron_F5E_Ramat:SetModex(20)
Squadron_F5E_Ramat:SetRadio(313)
Squadron_F5E_Ramat:SetSkill(AI.Skill.HIGH)
Squadron_F5E_Ramat:SetCallsign(CALLSIGN.Aircraft.Colt, 7)
Squadron_F5E_Ramat:SetMissionRange(120)
Squadron_F5E_Ramat:AddMissionCapability({
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.INTERCEPT,
    AUFTRAG.Type.BAI,
    AUFTRAG.Type.GROUNDATTACK })

Squadron_AJS37_Ramat = SQUADRON:New("AJS37 Template", math.random(2, 4), "Ramat AJS37")
Squadron_AJS37_Ramat:SetGrouping(2)
Squadron_AJS37_Ramat:SetDespawnAfterLanding()
Squadron_AJS37_Ramat:SetFuelLowThreshold(40)
Squadron_AJS37_Ramat:SetFuelLowRefuel(false)
Squadron_AJS37_Ramat:SetTurnoverTime(math.random(60, 120), math.random(30, 60))
Squadron_AJS37_Ramat:SetModex(30)
Squadron_AJS37_Ramat:SetRadio(313)
Squadron_AJS37_Ramat:SetSkill(AI.Skill.HIGH)
Squadron_AJS37_Ramat:SetCallsign(CALLSIGN.Aircraft.Springfield, 7)
Squadron_AJS37_Ramat:SetMissionRange(120)
Squadron_AJS37_Ramat:AddMissionCapability({
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.BAI,
    AUFTRAG.Type.GROUNDATTACK })

AirwingRamat = AIRWING:New("Warehouse Ramat-1", "Ramat Airwing")
AirwingRamat:SetAirbase(AIRBASE:FindByName(AIRBASE.Syria.Ramat_David))
AirwingRamat:SetTakeoffHot()
AirwingRamat:AddSquadron(Blue_Squadron_E2)
AirwingRamat:NewPayload("E2 Hawkeye", 2, {
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.AWACS })
AirwingRamat:AddSquadron(Squadron_F4E_Ramat)
AirwingRamat:NewPayload("F4E CAP", -1, {
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.GCICAP,
    AUFTRAG.Type.CAP,
    AUFTRAG.Type.INTERCEPT,
    AUFTRAG.Type.ESCORT })
AirwingRamat:NewPayload("F4E BAI", -1, {
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.BAI })
AirwingRamat:NewPayload("F4E SEAD", -1, {
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.SEAD })
AirwingRamat:AddSquadron(Squadron_F5E_Ramat)
AirwingRamat:NewPayload("F5E CAP Ramat", -1, {
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.INTERCEPT })
AirwingRamat:NewPayload("F5E BAI Ramat", -1, {
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.BAI })
AirwingRamat:AddSquadron(Squadron_AJS37_Ramat)
AirwingRamat:NewPayload("AJS37 Template", -1, {
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.BAI,
    AUFTRAG.Type.GROUNDATTACK })
AirwingRamat:AddSquadron(Squadron_A4E_Ramat)
AirwingRamat:NewPayload("A4E CAS Ramat", -1, {
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.BAI,
    AUFTRAG.Type.CAS,
    AUFTRAG.Type.CASENHANCED })
AirwingRamat:NewPayload("A4E SEAD Ramat", -1, {
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.SEAD })

function AirwingRamat:onafterFlightOnMission(From, Event, To, FlightGroup, Mission)
    local MissionStartMessage = string.format("%s  mission %s", FlightGroup:GetName(), Mission:GetType())
    trigger.action.outTextForCoalition(coalition.side.BLUE, MissionStartMessage, 15)
    trigger.action.outSoundForCoalition(coalition.side.BLUE, "walkietalkie2.ogg")
    env.info("OPL BlueChief" .. MissionStartMessage)
end

-- south
Squadron_UH1_South = SQUADRON:New("Blue Rotary Transport", 16, "South UH1H")
Squadron_UH1_South:SetGrouping(1)
Squadron_UH1_South:SetFuelLowThreshold(30)
Squadron_UH1_South:SetTurnoverTime(math.random(60, 120), math.random(30, 60))
Squadron_UH1_South:SetModex(217)
Squadron_UH1_South:SetRadio(228.50)
Squadron_UH1_South:SetSkill(GetRandomSkill())
Squadron_UH1_South:SetCallsign(CALLSIGN.Aircraft.Chevy, 2)
Squadron_UH1_South:SetMissionRange(125)
Squadron_UH1_South:SetAttribute(GROUP.Attribute.AIR_TRANSPORTHELO)
Squadron_UH1_South:AddMissionCapability({
    AUFTRAG.Type.HOVER,
    AUFTRAG.Type.OPSTRANSPORT,
    AUFTRAG.Type.TROOPTRANSPORT })

AirwingHeloSouth = AIRWING:New("Blue FARP London Warehouse", "Helo South Airwing")
AirwingHeloSouth:SetAirbase(AIRBASE:FindByName("Blue FARP London"))
AirwingHeloSouth:AddSquadron(Squadron_UH1_South)
AirwingHeloSouth:NewPayload("Blue Rotary Transport", -1, {
    AUFTRAG.Type.HOVER,
    AUFTRAG.Type.OPSTRANSPORT,
    AUFTRAG.Type.TROOPTRANSPORT })

Squadron_UH1_North = SQUADRON:New("Blue Rotary Transport", 16, "North UH1H")
Squadron_UH1_North:SetGrouping(1)
Squadron_UH1_North:SetFuelLowThreshold(30)
Squadron_UH1_North:SetTurnoverTime(math.random(60, 120), math.random(30, 60))
Squadron_UH1_North:SetModex(213)
Squadron_UH1_North:SetRadio(229.50)
Squadron_UH1_North:SetSkill(GetRandomSkill())
Squadron_UH1_North:SetCallsign(CALLSIGN.Aircraft.Pontiac, 2)
Squadron_UH1_North:SetMissionRange(125)
Squadron_UH1_North:SetAttribute(GROUP.Attribute.AIR_TRANSPORTHELO)
Squadron_UH1_North:AddMissionCapability({
    AUFTRAG.Type.HOVER,
    AUFTRAG.Type.OPSTRANSPORT,
    AUFTRAG.Type.TROOPTRANSPORT })

AirwingHeloNorth = AIRWING:New("Blue FARP Dallas Warehouse", "Helo North Airwing")
AirwingHeloNorth:SetAirbase(AIRBASE:FindByName("Blue FARP Dallas AI"))
AirwingHeloNorth:AddSquadron(Squadron_UH1_North)
AirwingHeloNorth:NewPayload("Blue Rotary Transport", -1, {
    AUFTRAG.Type.HOVER,
    AUFTRAG.Type.OPSTRANSPORT,
    AUFTRAG.Type.TROOPTRANSPORT })

-- Jirah
Squadron_MIG19_Jirah = SQUADRON:New("MiG19 Template", math.random(1, 2), "Jirah MiG19P")
Squadron_MIG19_Jirah:SetGrouping(math.random(2, 4))
Squadron_MIG19_Jirah:SetFuelLowThreshold(40)
Squadron_MIG19_Jirah:SetTurnoverTime(math.random(60, 120), math.random(30, 60))
Squadron_MIG19_Jirah:SetModex(50)
Squadron_MIG19_Jirah:SetRadio(124)
Squadron_MIG19_Jirah:SetSkill(GetRandomSkill())
Squadron_MIG19_Jirah:SetCallsign(CALLSIGN.Aircraft.Enfield, 2)
Squadron_MIG19_Jirah:SetMissionRange(120)
Squadron_MIG19_Jirah:AddMissionCapability({
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.BAI,
    AUFTRAG.Type.ESCORT })

Squadron_MIG19_Intercept_Jirah = SQUADRON:New("MiG19 Template", math.random(0, 2), "Jirah Intercept MiG19P")
Squadron_MIG19_Intercept_Jirah:SetGrouping(math.random(2, 4))
Squadron_MIG19_Intercept_Jirah:SetFuelLowThreshold(40)
Squadron_MIG19_Intercept_Jirah:SetTurnoverTime(math.random(60, 120), math.random(30, 60))
Squadron_MIG19_Intercept_Jirah:SetModex(50)
Squadron_MIG19_Intercept_Jirah:SetRadio(124)
Squadron_MIG19_Intercept_Jirah:SetSkill(GetRandomSkill())
Squadron_MIG19_Intercept_Jirah:SetCallsign(CALLSIGN.Aircraft.Enfield, 2)
Squadron_MIG19_Intercept_Jirah:SetMissionRange(120)
Squadron_MIG19_Intercept_Jirah:AddMissionCapability({
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.GCICAP,
    AUFTRAG.Type.INTERCEPT })

Squadron_MIG21_Jirah = SQUADRON:New("MiG21 Template-2", math.random(0, 2), "Jirah MiG21Bis")
Squadron_MIG21_Jirah:SetGrouping(math.random(2, 4))
Squadron_MIG21_Jirah:SetFuelLowThreshold(40)
Squadron_MIG21_Jirah:SetTurnoverTime(math.random(60, 120), math.random(30, 60))
Squadron_MIG21_Jirah:SetModex(040)
Squadron_MIG21_Jirah:SetRadio(124)
Squadron_MIG21_Jirah:SetSkill(GetRandomSkill())
Squadron_MIG21_Jirah:SetCallsign(CALLSIGN.Aircraft.Enfield, 3)
Squadron_MIG21_Jirah:SetMissionRange(100)
Squadron_MIG21_Jirah:AddMissionCapability({
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.GCICAP,
    AUFTRAG.Type.INTERCEPT })

AirwingJirah = AIRWING:New("Warehouse Jirah-1", "Airwing Jirah")
AirwingJirah:SetAirbase(AIRBASE:FindByName(AIRBASE.Syria.Abu_al_Jirah))
AirwingJirah:SetTakeoffAir()
AirwingJirah:AddSquadron(Squadron_MIG19_Jirah)
AirwingJirah:AddSquadron(Squadron_MIG19_Intercept_Jirah)
AirwingJirah:NewPayload("MiG19 BAI", -1, {
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.BAI })
AirwingJirah:NewPayload("MiG19 CAP", -1, {
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.ESCORT,
    AUFTRAG.Type.GCICAP,
    AUFTRAG.Type.INTERCEPT})
AirwingJirah:AddSquadron(Squadron_MIG21_Jirah)
AirwingJirah:NewPayload("MiG21 Template-2", -1, {
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.GCICAP,
    AUFTRAG.Type.INTERCEPT })

function AirwingJirah:onafterFlightOnMission(From, Event, To, FlightGroup, Mission)
    local MissionStartMessage = string.format("%s  mission %s", FlightGroup:GetName(), Mission:GetType())
    trigger.action.outTextForCoalition(coalition.side.RED, MissionStartMessage, 15)
    trigger.action.outSoundForCoalition(coalition.side.RED, "walkietalkie2.ogg")
    env.info("OPL RedChief" .. MissionStartMessage)
end

-- shayrat
Squadron_MIG19_Shayrat = SQUADRON:New("MiG19 Template", math.random(2, 4), "Shayrat MiG19P")
Squadron_MIG19_Shayrat:SetGrouping(math.random(2, 4))
Squadron_MIG19_Shayrat:SetFuelLowThreshold(40)
Squadron_MIG19_Shayrat:SetTurnoverTime(math.random(60, 120), math.random(30, 60))
Squadron_MIG19_Shayrat:SetModex(030)
Squadron_MIG19_Shayrat:SetRadio(124)
Squadron_MIG19_Shayrat:SetSkill(GetRandomSkill())
Squadron_MIG19_Shayrat:SetCallsign(CALLSIGN.Aircraft.Springfield, 3)
Squadron_MIG19_Shayrat:SetMissionRange(120)
Squadron_MIG19_Shayrat:AddMissionCapability({
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.BAI,
    AUFTRAG.Type.ESCORT })

Squadron_MIG19_Intercept_Shayrat = SQUADRON:New("MiG19 Template", math.random(0, 2), "Shayrat Intercept MiG19P")
Squadron_MIG19_Intercept_Shayrat:SetGrouping(math.random(2, 4))
Squadron_MIG19_Intercept_Shayrat:SetFuelLowThreshold(40)
Squadron_MIG19_Intercept_Shayrat:SetTurnoverTime(math.random(60, 120), math.random(30, 60))
Squadron_MIG19_Intercept_Shayrat:SetModex(030)
Squadron_MIG19_Intercept_Shayrat:SetRadio(124)
Squadron_MIG19_Intercept_Shayrat:SetSkill(GetRandomSkill())
Squadron_MIG19_Intercept_Shayrat:SetCallsign(CALLSIGN.Aircraft.Springfield, 3)
Squadron_MIG19_Intercept_Shayrat:SetMissionRange(120)
Squadron_MIG19_Intercept_Shayrat:AddMissionCapability({
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.GCICAP,
    AUFTRAG.Type.INTERCEPT })

Squadron_MIG21_Shayrat = SQUADRON:New("MiG21 Template-1", math.random(0, 2), "Shayrat MiG21Bis")
Squadron_MIG21_Shayrat:SetGrouping(math.random(2, 4))
Squadron_MIG21_Shayrat:SetFuelLowThreshold(40)
Squadron_MIG21_Shayrat:SetTurnoverTime(math.random(60, 120), math.random(30, 60))
Squadron_MIG21_Shayrat:SetModex(020)
Squadron_MIG21_Shayrat:SetRadio(124)
Squadron_MIG21_Shayrat:SetSkill(GetRandomSkill())
Squadron_MIG21_Shayrat:SetCallsign(CALLSIGN.Aircraft.Springfield, 4)
Squadron_MIG21_Shayrat:SetMissionRange(100)
Squadron_MIG21_Shayrat:AddMissionCapability({
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.GCICAP,
    AUFTRAG.Type.INTERCEPT })

AirwingShayrat = AIRWING:New("Warehouse Shayrat-1", "Airwing Shayrat")
AirwingShayrat:SetAirbase(AIRBASE:FindByName(AIRBASE.Syria.Shayrat))
AirwingShayrat:SetTakeoffAir()
AirwingShayrat:AddSquadron(Squadron_MIG19_Shayrat)
AirwingShayrat:AddSquadron(Squadron_MIG19_Intercept_Shayrat)
AirwingShayrat:NewPayload("MiG19 BAI", -1, {
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.BAI })
AirwingShayrat:NewPayload("MiG19 CAP", -1, {
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.GCICAP,
    AUFTRAG.Type.INTERCEPT,
    AUFTRAG.Type.ESCORT })
AirwingShayrat:AddSquadron(Squadron_MIG21_Shayrat)
AirwingShayrat:NewPayload("MiG21 Template-1", -1, {
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.GCICAP,
    AUFTRAG.Type.INTERCEPT })

function AirwingShayrat:onafterFlightOnMission(From, Event, To, FlightGroup, Mission)
    local MissionStartMessage = string.format("%s  mission %s", FlightGroup:GetName(), Mission:GetType())
    trigger.action.outTextForCoalition(coalition.side.RED, MissionStartMessage, 15)
    trigger.action.outSoundForCoalition(coalition.side.RED, "walkietalkie2.ogg")
    env.info("OPL RedChief" .. MissionStartMessage)
end

-- sayqal
Squadron_MIG21_Sayqal = SQUADRON:New("MiG21 Template-1", math.random(1, 2), "Sayqal MiG21Bis")
Squadron_MIG21_Sayqal:SetGrouping(math.random(2, 4))
Squadron_MIG21_Sayqal:SetFuelLowThreshold(40)
Squadron_MIG21_Sayqal:SetTurnoverTime(math.random(60, 120), math.random(30, 60))
Squadron_MIG21_Sayqal:SetModex(010)
Squadron_MIG21_Sayqal:SetRadio(124)
Squadron_MIG21_Sayqal:SetSkill(GetRandomSkill())
Squadron_MIG21_Sayqal:SetCallsign(CALLSIGN.Aircraft.Uzi, 4)
Squadron_MIG21_Sayqal:SetMissionRange(100)
Squadron_MIG21_Sayqal:AddMissionCapability({
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.GCICAP,
    AUFTRAG.Type.INTERCEPT,
    AUFTRAG.Type.ESCORT })

Squadron_Su17_Sayqal = SQUADRON:New("Su17 Template", math.random(0, 2), "Sayqal Su17")
Squadron_Su17_Sayqal:SetGrouping(math.random(2, 4))
Squadron_Su17_Sayqal:SetFuelLowThreshold(30)
Squadron_Su17_Sayqal:SetTurnoverTime(math.random(60, 120), math.random(30, 60))
Squadron_Su17_Sayqal:SetModex(60)
Squadron_Su17_Sayqal:SetRadio(124)
Squadron_Su17_Sayqal:SetSkill(GetRandomSkill())
Squadron_Su17_Sayqal:SetCallsign(CALLSIGN.Aircraft.Uzi, 6)
Squadron_Su17_Sayqal:SetMissionRange(100)
Squadron_Su17_Sayqal:AddMissionCapability({
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.BAI,
    AUFTRAG.Type.GROUNDATTACK })

AirwingSayqal = AIRWING:New("Warehouse Sayqal-1", "Airwing Sayqal")
AirwingSayqal:SetAirbase(AIRBASE:FindByName(AIRBASE.Syria.Sayqal))
AirwingSayqal:SetTakeoffAir()
AirwingSayqal:AddSquadron(Squadron_MIG21_Sayqal)
AirwingSayqal:NewPayload("MiG21 Template-1", -1, {
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.GCICAP,
    AUFTRAG.Type.INTERCEPT,
    AUFTRAG.Type.ESCORT })
AirwingSayqal:AddSquadron(Squadron_Su17_Sayqal)
AirwingSayqal:NewPayload("Su17 Template", -1, {
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.BAI,
    AUFTRAG.Type.GROUNDATTACK })

function AirwingSayqal:onafterFlightOnMission(From, Event, To, FlightGroup, Mission)
    local MissionStartMessage = string.format("%s  mission %s", FlightGroup:GetName(), Mission:GetType())
    trigger.action.outTextForCoalition(coalition.side.RED, MissionStartMessage, 15)
    trigger.action.outSoundForCoalition(coalition.side.RED, "walkietalkie2.ogg")
    env.info("OPL RedChief" .. MissionStartMessage)
end

-- taftanaz
local Taftanaz_Mi8_SquadronName = "Taftanaz Mi8MTV2"
Squadron_MI8_North = SQUADRON:New("Red Rotary Transport", 16, Taftanaz_Mi8_SquadronName)
Squadron_MI8_North:SetGrouping(1)
Squadron_MI8_North:SetFuelLowThreshold(30)
Squadron_MI8_North:SetTurnoverTime(math.random(60, 120), math.random(30, 60))
Squadron_MI8_North:SetModex(217)
Squadron_MI8_North:SetRadio(124)
Squadron_MI8_North:SetSkill(GetRandomSkill())
Squadron_MI8_North:SetCallsign(CALLSIGN.Aircraft.Pontiac, 2)
Squadron_MI8_North:SetMissionRange(125)
Squadron_MI8_North:SetAttribute(GROUP.Attribute.AIR_TRANSPORTHELO)
Squadron_MI8_North:AddMissionCapability(
        { AUFTRAG.Type.ORBIT, AUFTRAG.Type.HOVER, AUFTRAG.Type.OPSTRANSPORT, AUFTRAG.Type.TROOPTRANSPORT })

AirwingTaftanaz = AIRWING:New("Red Taftanaz Warehouse-1", "Taftanaz Rotary Airwing")
AirwingTaftanaz:SetAirbase(AIRBASE:FindByName(AIRBASE.Syria.Taftanaz))
AirwingTaftanaz:AddSquadron(Squadron_MI8_North)
AirwingTaftanaz:NewPayload("Red Rotary Transport", -1, {
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.HOVER,
    AUFTRAG.Type.OPSTRANSPORT,
    AUFTRAG.Type.TROOPTRANSPORT })

function AirwingTaftanaz:onafterFlightOnMission(From, Event, To, FlightGroup, Mission)
    local MissionStartMessage = string.format("%s  mission %s", FlightGroup:GetName(), Mission:GetType())
    trigger.action.outTextForCoalition(coalition.side.RED, MissionStartMessage, 15)
    trigger.action.outSoundForCoalition(coalition.side.RED, "walkietalkie2.ogg")
    env.info("OPL RedChief" .. MissionStartMessage)
end

-- mezzeh
local Mezzeh_Mi8_SquadronName = "Mezzeh Mi8MTV2"
Squadron_MI8_South = SQUADRON:New("Red Rotary Transport", 16, Mezzeh_Mi8_SquadronName)
Squadron_MI8_South:SetGrouping(1)
Squadron_MI8_South:SetFuelLowThreshold(30)
Squadron_MI8_South:SetTurnoverTime(math.random(60, 120), math.random(30, 60))
Squadron_MI8_South:SetModex(217)
Squadron_MI8_South:SetRadio(124)
Squadron_MI8_South:SetSkill(GetRandomSkill())
Squadron_MI8_South:SetCallsign(CALLSIGN.Aircraft.Pontiac, 2)
Squadron_MI8_South:SetMissionRange(125)
Squadron_MI8_South:SetAttribute(GROUP.Attribute.AIR_TRANSPORTHELO)
Squadron_MI8_South:AddMissionCapability(
        { AUFTRAG.Type.ORBIT, AUFTRAG.Type.HOVER, AUFTRAG.Type.OPSTRANSPORT, AUFTRAG.Type.TROOPTRANSPORT })

AirwingMezzeh = AIRWING:New("Red Mezzeh Warehouse-1", "Mezzeh Rotary Airwing")
AirwingMezzeh:SetAirbase(AIRBASE:FindByName(AIRBASE.Syria.Mezzeh))
AirwingMezzeh:AddSquadron(Squadron_MI8_South)
AirwingMezzeh:NewPayload("Red Rotary Transport", -1, {
    AUFTRAG.Type.ORBIT,
    AUFTRAG.Type.HOVER,
    AUFTRAG.Type.OPSTRANSPORT,
    AUFTRAG.Type.TROOPTRANSPORT })

function AirwingMezzeh:onafterFlightOnMission(From, Event, To, FlightGroup, Mission)
    local MissionStartMessage = string.format("%s  mission %s", FlightGroup:GetName(), Mission:GetType())
    trigger.action.outTextForCoalition(coalition.side.RED, MissionStartMessage, 15)
    trigger.action.outSoundForCoalition(coalition.side.RED, "walkietalkie2.ogg")
    env.info("OPL RedChief" .. MissionStartMessage)
end

ZZZLoaderAssert["2-airwings"] = true