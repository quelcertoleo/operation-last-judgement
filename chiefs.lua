--[[
k: #MARKER
v: #Wrapper.Group#GROUP
]]
local MapContactMarks = {}

-- zones
local ZoneBlueBorder = ZONE_POLYGON:FindByName("Blue Airspace")
local ZoneBlueBorderSet = SET_ZONE:New()
ZoneBlueBorderSet:AddZone(ZoneBlueBorder)

local ZoneRedBorder = ZONE_POLYGON:FindByName("Red Airspace")
local ZoneRedBorderSet = SET_ZONE:New()
ZoneRedBorderSet:AddZone(ZoneRedBorder)

-- chiefs
-- BLUE
local BlueDetection = SET_GROUP:New():FilterCoalitions("blue"):FilterStart()
BlueChiefSouth = CHIEF:New(coalition.side.BLUE, BlueDetection, "Blue Chief South")
--SetDetectionTypes(DetectVisual, DetectOptical, DetectRadar, DetectIRST, DetectRWR, DetectDLINK)
BlueChiefSouth:SetDetectionTypes(true, true, true, false, true, true)
BlueChiefSouth:SetDetectStatics(true) -- not sure if enable or not (fry meme)
BlueChiefSouth:SetClusterAnalysis(true, false, false)
BlueChiefSouth:SetClusterRadius(5)
BlueChiefSouth:AddAwacsZone(ZONE_POLYGON:FindByName("E2 Zone"), 20000, 200, 0, 25)
BlueChiefSouth:SetBorderZones(ZoneBlueBorderSet)
BlueChiefSouth:AddConflictZone(ZONE_POLYGON:FindByName("Zone Sunshine Valley"))
--BlueChief:AddTankerZone(ZONE_POLYGON:FindByName("Zone Refuel"), 22000, 320, 180, 50, 0) -- 0 = boom
--BlueChief:AddTankerZone(ZONE_POLYGON:FindByName("Zone Refuel"), 15000, 160, 90, 30, 1) -- 1 = probe

-- chief does not detect Group.Category.HELICOPTER <--- UNCOMMENT BELOW
BlueChiefSouth:SetFilterCategory(Group.Category.AIRPLANE, Group.Category.GROUND, Group.Category.SHIP)

-- a2a
BlueChiefSouth:SetLimitMission(2, AUFTRAG.Type.CAP)
BlueChiefSouth:SetLimitMission(2, AUFTRAG.Type.GCICAP)
BlueChiefSouth:SetLimitMission(0, AUFTRAG.Type.ESCORT)
BlueChiefSouth:SetLimitMission(2, AUFTRAG.Type.INTERCEPT)
-- a2g
BlueChiefSouth:SetLimitMission(2, AUFTRAG.Type.BAI)
BlueChiefSouth:SetLimitMission(2, AUFTRAG.Type.BOMBCARPET)
BlueChiefSouth:SetLimitMission(2, AUFTRAG.Type.BOMBING)
BlueChiefSouth:SetLimitMission(2, AUFTRAG.Type.CAS)
BlueChiefSouth:SetLimitMission(2, AUFTRAG.Type.CASENHANCED)
BlueChiefSouth:SetLimitMission(2, AUFTRAG.Type.GROUNDATTACK)
BlueChiefSouth:SetLimitMission(2, AUFTRAG.Type.SEAD)
BlueChiefSouth:SetLimitMission(2, AUFTRAG.Type.STRIKE)
-- ground
BlueChiefSouth:SetLimitMission(4, AUFTRAG.Type.ARTY)
BlueChiefSouth:SetLimitMission(20, AUFTRAG.Type.ARMOREDGUARD)
BlueChiefSouth:SetLimitMission(2, AUFTRAG.Type.GROUNDESCORT)
BlueChiefSouth:SetLimitMission(20, AUFTRAG.Type.NOTHING)
BlueChiefSouth:SetLimitMission(20, AUFTRAG.Type.ONGUARD)
BlueChiefSouth:SetLimitMission(20, AUFTRAG.Type.OPSTRANSPORT)
BlueChiefSouth:SetLimitMission(20, AUFTRAG.Type.TROOPTRANSPORT)
BlueChiefSouth:SetLimitMission(4, AUFTRAG.Type.GROUNDATTACK)
BlueChiefSouth:SetLimitMission(8, AUFTRAG.Type.PATROLZONE)
BlueChiefSouth:SetLimitMission(16, AUFTRAG.Type.CAPTUREZONE)

BlueChiefSouth:AddAirwing(AirwingRamat)
BlueChiefSouth:AddAirwing(AirwingHeloSouth)

BlueChiefSouth:AddBrigade(BlueBrigadeSouth)
BlueChiefSouth:AddBrigade(BlueTroopsBrigadeSouth)

local BlueResourceEmpty, BlueResourceInf = BlueChiefSouth:CreateResource(AUFTRAG.Type.ONGUARD, 2, 2, GROUP.Attribute.GROUND_INFANTRY)
BlueChiefSouth:AddTransportToResource(BlueResourceInf, 1, 2, { GROUP.Attribute.AIR_TRANSPORTHELO })

local BlueResourceSouthOccupied, BlueResourceSouthCas = BlueChiefSouth:CreateResource(AUFTRAG.Type.CASENHANCED, 2, 4)
BlueChiefSouth:AddToResource(BlueResourceSouthOccupied, AUFTRAG.Type.ARMOREDGUARD, 1, 1)

BlueChiefSouth:SetResponseOnTarget(1, 1, 1, TARGET.Category.GROUND, AUFTRAG.Type.BAI, 4, nil, CHIEF.Strategy.OFFENSIVE)

BlueChiefSouth:AddStrategicZone(ZoneDelta_Ops, 100, 1, BlueResourceSouthOccupied, BlueResourceEmpty)
BlueChiefSouth:AddStrategicZone(ZoneRayak_Ops, 100, 2, BlueResourceSouthOccupied, BlueResourceEmpty)
BlueChiefSouth:AddStrategicZone(ZoneCharlie_Ops, 100, 3, BlueResourceSouthOccupied, BlueResourceEmpty)
BlueChiefSouth:AddStrategicZone(ZoneBeta_Ops, 100, 4, BlueResourceSouthOccupied, BlueResourceEmpty)
BlueChiefSouth:AddStrategicZone(ZoneAlfa_Ops, 100, 5, BlueResourceSouthOccupied, BlueResourceEmpty)

BlueChiefSouth:AddRejectZone(ZONE_POLYGON:FindByName("Red Airspace"))

BlueChiefSouth:SetStrategy(CHIEF.Strategy.OFFENSIVE)

-- start delay to wait for opszones states, especially when reloading mission with DSMC
TIMER:New(function()
    BlueChiefSouth:Start()
end) :Start(120)

function BlueChiefSouth:OnAfterZoneCaptured(From, Event, To, OpsZone)
    local CaptureMessage = string.format("%s captured %s", OpsZone:GetOwnerName(), OpsZone:GetName())
    trigger.action.outTextForCoalition(coalition.side.BLUE, CaptureMessage, 15)
    trigger.action.outSoundForCoalition(coalition.side.BLUE, "walkietalkie.ogg")
    env.info("OPL BlueChiefSouth:OnAfterZoneCaptured -> " .. CaptureMessage)

    BlueBrigadeSouth:SetSpawnZone(GetBlueChiefSouthFrontlineSpawn(), UTILS.NMToMeters(100))

    local RandomCoordinate = OpsZone:GetRandomCoordinate()
    GroupsInZoneGoToCoordinate(OpsZone, RandomCoordinate)

    SpawnBlueCamp(OpsZone, RandomCoordinate)
end

function BlueChiefSouth:OnAfterZoneAttacked(From, Event, To, OpsZone)
    local AttackMessage = string.format("%s under attack", OpsZone:GetName())
    trigger.action.outTextForCoalition(coalition.side.BLUE, AttackMessage, 15)
    trigger.action.outSoundForCoalition(coalition.side.BLUE, "walkietalkie.ogg")

    env.info("OPL BlueChiefSouth:OnAfterZoneAttacked -> " .. AttackMessage)
end

function BlueChiefSouth:OnAfterZoneLost(From, Event, To, OpsZone)
    local LostMessage = string.format("%s captured by enemy", OpsZone:GetName())
    trigger.action.outTextForCoalition(coalition.side.BLUE, LostMessage, 15)
    trigger.action.outSoundForCoalition(coalition.side.BLUE, "walkietalkie.ogg")
    env.info("OPL BlueChiefSouth:OnAfterZoneLost -> " .. OpsZone:GetName())

    if OpsZone:GetPreviousOwner() == coalition.side.BLUE then
        BlueBrigadeSouth:SetSpawnZone(GetBlueChiefSouthPreviousSpawnZone(), UTILS.NMToMeters(100))
    end
end

function BlueChiefSouth:OnAfterZoneEmpty(From, Event, To, OpsZone)
    local EmptyMessage = string.format("Zone %s is empty.", OpsZone:GetName())
    trigger.action.outTextForCoalition(coalition.side.BLUE, EmptyMessage, 15)
    trigger.action.outSoundForCoalition(coalition.side.BLUE, "walkietalkie.ogg")
    env.info("OPL BlueChiefSouth:OnAfterZoneEmpty -> " .. EmptyMessage)

    local RandomCoordinate = OpsZone:GetRandomCoordinate()
    local ArmouredGuardMission = AUFTRAG:NewARMOREDGUARD(RandomCoordinate)
    BlueChiefSouth:AddMission(ArmouredGuardMission)

    local ZoneBase = OpsZone:GetZone()
    ZoneBase:RemoveJunk()
end

function AirwingRamat:OnAfterFlightOnMission(From, Event, To, FlightGroup, Mission)
    if (Mission:GetType() == AUFTRAG.Type.BAI or Mission:GetType() == AUFTRAG.Type.CASENHANCED) then
        Mission:SetMissionSpeed(math.random(250, 280))
        local GroupToEscort = FlightGroup:GetGroup()
        local EscortMission = AUFTRAG:NewGCICAP(Mission:GetTargetCoordinate(), math.random(10000, 18000), 280, math.random(0, 359), math.random(10, 20))
        BlueChiefSouth:AddMission(EscortMission)
        TIMER:New(function()
            Mission:Success()
            EscortMission:Success()
        end) :Start(math.random(1800, 2400))
    end
end

function BlueChiefSouth:OnAfterNewContact(From, Event, To, Contact)
    -- Gather info of contact.
    local ContactName = BlueChiefSouth:GetContactName(Contact)
    local ContactType = BlueChiefSouth:GetContactTypeName(Contact)
    local ContactThreat = BlueChiefSouth:GetContactThreatlevel(Contact)
    local ContactGroup = BlueChiefSouth:GetContactGroup(Contact)
    if ContactGroup:IsGround() or ContactGroup:IsShip() then
        local ContactCoordinate = ContactGroup:GetCoord()
        local MapMarker = MARKER:New(ContactCoordinate, ContactType):ReadOnly():ToBlue()
        MapContactMarks[MapMarker] = ContactGroup
    end
    -- see attributes on GROUP.Attribute
    local ContactAttribute = ContactGroup:GetAttribute()

    if ContactType == "Su-17M4" then
        local InterceptMission = AUFTRAG:NewGCICAP(ZONE:FindByName("Zone Sunshine Valley"), math.random(10000, 15000), math.random(250, 300), math.random(0, 359), math.random(10, 20))
        BlueChiefSouth:AddMission(InterceptMission)
    end

    local text = string.format("OPL BlueChiefSouth detected NEW contact: Name=%s, Type=%s, Threat Level=%d, Attribute=%s", ContactName, ContactType, ContactThreat, ContactAttribute)
    env.info(text)
end

-- BLUE CHIEF North
BlueChiefNorth = CHIEF:New(coalition.side.BLUE, BlueDetection, "Blue Chief")
--SetDetectionTypes(DetectVisual, DetectOptical, DetectRadar, DetectIRST, DetectRWR, DetectDLINK)
BlueChiefNorth:SetDetectionTypes(true, true, true, false, true, true)
BlueChiefNorth:SetDetectStatics(true) -- not sure if enable or not (fry meme)
BlueChiefNorth:SetClusterAnalysis(true, false, false)
BlueChiefNorth:SetClusterRadius(5)
BlueChiefNorth:SetBorderZones(ZoneBlueBorderSet)
BlueChiefNorth:AddConflictZone(ZONE_POLYGON:FindByName("Zone Bad Hollow"))
--BlueChief:AddTankerZone(ZONE_POLYGON:FindByName("Zone Refuel"), 22000, 320, 180, 50, 0) -- 0 = boom
--BlueChief:AddTankerZone(ZONE_POLYGON:FindByName("Zone Refuel"), 15000, 160, 90, 30, 1) -- 1 = probe

-- chief does not detect Group.Category.HELICOPTER <--- UNCOMMENT BELOW
BlueChiefNorth:SetFilterCategory(Group.Category.AIRPLANE, Group.Category.GROUND, Group.Category.SHIP)

-- a2a
BlueChiefNorth:SetLimitMission(2, AUFTRAG.Type.CAP)
BlueChiefNorth:SetLimitMission(2, AUFTRAG.Type.GCICAP)
BlueChiefNorth:SetLimitMission(0, AUFTRAG.Type.ESCORT)
BlueChiefNorth:SetLimitMission(2, AUFTRAG.Type.INTERCEPT)
-- a2g
BlueChiefNorth:SetLimitMission(2, AUFTRAG.Type.BAI)
BlueChiefNorth:SetLimitMission(2, AUFTRAG.Type.BOMBCARPET)
BlueChiefNorth:SetLimitMission(2, AUFTRAG.Type.BOMBING)
BlueChiefNorth:SetLimitMission(2, AUFTRAG.Type.CAS)
BlueChiefNorth:SetLimitMission(2, AUFTRAG.Type.CASENHANCED)
BlueChiefNorth:SetLimitMission(2, AUFTRAG.Type.GROUNDATTACK)
BlueChiefNorth:SetLimitMission(2, AUFTRAG.Type.SEAD)
BlueChiefNorth:SetLimitMission(2, AUFTRAG.Type.STRIKE)
-- ground
BlueChiefNorth:SetLimitMission(4, AUFTRAG.Type.ARTY)
BlueChiefNorth:SetLimitMission(20, AUFTRAG.Type.ARMOREDGUARD)
BlueChiefNorth:SetLimitMission(2, AUFTRAG.Type.GROUNDESCORT)
BlueChiefNorth:SetLimitMission(20, AUFTRAG.Type.NOTHING)
BlueChiefNorth:SetLimitMission(20, AUFTRAG.Type.ONGUARD)
BlueChiefNorth:SetLimitMission(20, AUFTRAG.Type.OPSTRANSPORT)
BlueChiefNorth:SetLimitMission(20, AUFTRAG.Type.TROOPTRANSPORT)
BlueChiefNorth:SetLimitMission(4, AUFTRAG.Type.GROUNDATTACK)
BlueChiefNorth:SetLimitMission(8, AUFTRAG.Type.PATROLZONE)
BlueChiefNorth:SetLimitMission(16, AUFTRAG.Type.CAPTUREZONE)

BlueChiefNorth:AddAirwing(AirwingIncirlik)
BlueChiefNorth:AddAirwing(AirwingHeloNorth)

BlueChiefNorth:AddBrigade(BlueBrigadeNorth)
BlueChiefNorth:AddBrigade(BlueTroopsBrigadeNorth)

local BlueResourceEmptyNorth, BlueResourceInfNorth = BlueChiefNorth:CreateResource(AUFTRAG.Type.ONGUARD, 2, 2, GROUP.Attribute.GROUND_INFANTRY)
BlueChiefNorth:AddTransportToResource(BlueResourceInfNorth, 1, 2, { GROUP.Attribute.AIR_TRANSPORTHELO })

local BlueResourceOccupiedNorth, BlueResourceCasZoneNorth = BlueChiefNorth:CreateResource(AUFTRAG.Type.CASENHANCED, 2, 4)
BlueChiefNorth:AddToResource(BlueResourceOccupiedNorth, AUFTRAG.Type.ARMOREDGUARD, 1, 1)

BlueChiefNorth:SetResponseOnTarget(1, 1, 1, TARGET.Category.GROUND, AUFTRAG.Type.BAI, 4, nil, CHIEF.Strategy.OFFENSIVE)

BlueChiefNorth:AddStrategicZone(ZoneZulu_Ops, 100, 1, BlueResourceOccupiedNorth, BlueResourceEmptyNorth)
BlueChiefNorth:AddStrategicZone(ZoneYankee_Ops, 100, 2, BlueResourceOccupiedNorth, BlueResourceEmptyNorth)
BlueChiefNorth:AddStrategicZone(ZoneXRay_Ops, 100, 3, BlueResourceOccupiedNorth, BlueResourceEmptyNorth)
BlueChiefNorth:AddStrategicZone(ZoneWhiskey_Ops, 100, 4, BlueResourceOccupiedNorth, BlueResourceEmptyNorth)
BlueChiefNorth:AddStrategicZone(ZoneTaftanaz_Ops, 100, 5, BlueResourceOccupiedNorth, BlueResourceEmptyNorth)

BlueChiefNorth:AddRejectZone(ZONE_POLYGON:FindByName("Red Airspace"))

BlueChiefNorth:SetStrategy(CHIEF.Strategy.OFFENSIVE)

-- start delay to wait for opszones states, especially when reloading mission with DSMC
TIMER:New(function()
    BlueChiefNorth:Start()
end) :Start(120)

function BlueChiefNorth:OnAfterZoneCaptured(From, Event, To, OpsZone)
    local CaptureMessage = string.format("%s captured %s", OpsZone:GetOwnerName(), OpsZone:GetName())
    trigger.action.outTextForCoalition(coalition.side.BLUE, CaptureMessage, 15)
    trigger.action.outSoundForCoalition(coalition.side.BLUE, "walkietalkie.ogg")
    env.info("OPL BlueChiefNorth:OnAfterZoneCaptured -> " .. CaptureMessage)

    BlueBrigadeNorth:SetSpawnZone(GetBlueChiefNorthFrontlineSpawn(), UTILS.NMToMeters(100))

    local RandomCoordinate = OpsZone:GetRandomCoordinate()
    GroupsInZoneGoToCoordinate(OpsZone, RandomCoordinate)

    SpawnBlueCamp(OpsZone, RandomCoordinate)
end

function BlueChiefNorth:OnAfterZoneAttacked(From, Event, To, OpsZone)
    local AttackMessage = string.format("%s under attack", OpsZone:GetName())
    trigger.action.outTextForCoalition(coalition.side.BLUE, AttackMessage, 15)
    trigger.action.outSoundForCoalition(coalition.side.BLUE, "walkietalkie.ogg")
    env.info("OPL BlueChiefNorth:OnAfterZoneAttacked -> " .. AttackMessage)
end

function BlueChiefNorth:OnAfterZoneLost(From, Event, To, OpsZone)
    local LostMessage = string.format("%s captured by enemy", OpsZone:GetName())
    trigger.action.outTextForCoalition(coalition.side.BLUE, LostMessage, 15)
    trigger.action.outSoundForCoalition(coalition.side.BLUE, "walkietalkie.ogg")
    env.info("OPL BlueChiefNorth:OnAfterZoneLost -> " .. OpsZone:GetName())

    if OpsZone:GetPreviousOwner() == coalition.side.BLUE then
        BlueBrigadeNorth:SetSpawnZone(GetBlueChiefNorthPreviousSpawnZone(), UTILS.NMToMeters(100))
    end
end

function BlueChiefNorth:OnAfterZoneEmpty(From, Event, To, OpsZone)
    local EmptyMessage = string.format("Zone %s is empty.", OpsZone:GetName())
    trigger.action.outTextForCoalition(coalition.side.BLUE, EmptyMessage, 15)
    trigger.action.outSoundForCoalition(coalition.side.BLUE, "walkietalkie.ogg")
    env.info("OPL BlueChiefNorth:OnAfterZoneEmpty -> " .. EmptyMessage)

    local RandomCoordinate = OpsZone:GetRandomCoordinate()
    local ArmouredGuardMission = AUFTRAG:NewARMOREDGUARD(RandomCoordinate)
    BlueChiefNorth:AddMission(ArmouredGuardMission)

    local ZoneBase = OpsZone:GetZone()
    ZoneBase:RemoveJunk()
end

function BlueChiefNorth:OnAfterNewContact(From, Event, To, Contact)
    -- Gather info of contact.
    local ContactName = BlueChiefNorth:GetContactName(Contact)
    local ContactType = BlueChiefNorth:GetContactTypeName(Contact)
    local ContactThreat = BlueChiefNorth:GetContactThreatlevel(Contact)
    local ContactGroup = BlueChiefNorth:GetContactGroup(Contact)
    if ContactGroup:IsGround() or ContactGroup:IsShip() then
        local ContactCoordinate = ContactGroup:GetCoord()
        local MapMarker = MARKER:New(ContactCoordinate, ContactType):ReadOnly():ToBlue()
        MapContactMarks[MapMarker] = ContactGroup
    end
    -- see attributes on GROUP.Attribute
    local ContactAttribute = ContactGroup:GetAttribute()

    local text = string.format("OPL BlueChiefNorth detected NEW contact: Name=%s, Type=%s, Threat Level=%d, Attribute=%s", ContactName, ContactType, ContactThreat, ContactAttribute)
    env.info(text)
end

function AirwingIncirlik:OnAfterFlightOnMission(From, Event, To, FlightGroup, Mission)
    if (Mission:GetType() == AUFTRAG.Type.BAI or Mission:GetType() == AUFTRAG.Type.CASENHANCED) then
        Mission:SetMissionSpeed(math.random(250, 280))
        local GroupToEscort = FlightGroup:GetGroup()
        local EscortMission = AUFTRAG:NewGCICAP(Mission:GetTargetCoordinate(), math.random(10000, 18000), 280, math.random(0, 359), math.random(10, 20))
        BlueChiefNorth:AddMission(EscortMission)
        TIMER:New(function()
            Mission:Success()
        end) :Start(math.random(1200, 1800))
        TIMER:New(function()
            EscortMission:Success()
        end) :Start(math.random(1800, 2200))
    end
end

-- RED
local RedDetection = SET_GROUP:New():FilterCoalitions("red"):FilterStart()
RedChiefSouth = CHIEF:New(coalition.side.RED, RedDetection, "Red Chief")
--SetDetectionTypes(DetectVisual, DetectOptical, DetectRadar, DetectIRST, DetectRWR, DetectDLINK)
RedChiefSouth:SetDetectionTypes(true, true, true, true, true, false)
RedChiefSouth:SetDetectStatics(false) -- not sure if enable or not (fry meme)
RedChiefSouth:SetClusterAnalysis(true, false, false)
RedChiefSouth:SetClusterRadius(20)
RedChiefSouth:SetBorderZones(ZoneRedBorderSet)
RedChiefSouth:AddConflictZone(ZONE_POLYGON:FindByName("Zone Sunshine Valley"))
RedChiefSouth:AddRejectZone(ZoneBlueBorder)
-- chief does not detect Group.Category.HELICOPTER <--- UNCOMMENT BELOW
RedChiefSouth:SetFilterCategory(Group.Category.AIRPLANE, Group.Category.GROUND, Group.Category.SHIP)

-- a2a
RedChiefSouth:SetLimitMission(0, AUFTRAG.Type.CAP)
RedChiefSouth:SetLimitMission(0, AUFTRAG.Type.GCICAP)
RedChiefSouth:SetLimitMission(1, AUFTRAG.Type.ESCORT)
RedChiefSouth:SetLimitMission(1, AUFTRAG.Type.INTERCEPT)
-- a2g
RedChiefSouth:SetLimitMission(2, AUFTRAG.Type.BAI)
RedChiefSouth:SetLimitMission(2, AUFTRAG.Type.BOMBCARPET)
RedChiefSouth:SetLimitMission(2, AUFTRAG.Type.BOMBING)
RedChiefSouth:SetLimitMission(2, AUFTRAG.Type.CAS)
RedChiefSouth:SetLimitMission(2, AUFTRAG.Type.CASENHANCED)
RedChiefSouth:SetLimitMission(2, AUFTRAG.Type.GROUNDATTACK)
RedChiefSouth:SetLimitMission(2, AUFTRAG.Type.SEAD)
RedChiefSouth:SetLimitMission(2, AUFTRAG.Type.STRIKE)
-- ground
RedChiefSouth:SetLimitMission(4, AUFTRAG.Type.ARTY)
RedChiefSouth:SetLimitMission(20, AUFTRAG.Type.ARMOREDGUARD)
RedChiefSouth:SetLimitMission(2, AUFTRAG.Type.GROUNDESCORT)
RedChiefSouth:SetLimitMission(20, AUFTRAG.Type.NOTHING)
RedChiefSouth:SetLimitMission(20, AUFTRAG.Type.ONGUARD)
RedChiefSouth:SetLimitMission(20, AUFTRAG.Type.OPSTRANSPORT)
RedChiefSouth:SetLimitMission(20, AUFTRAG.Type.TROOPTRANSPORT)
RedChiefSouth:SetLimitMission(4, AUFTRAG.Type.GROUNDATTACK)
RedChiefSouth:SetLimitMission(8, AUFTRAG.Type.PATROLZONE)
RedChiefSouth:SetLimitMission(16, AUFTRAG.Type.CAPTUREZONE)

RedChiefSouth:AddAirwing(AirwingShayrat)
RedChiefSouth:AddAirwing(AirwingSayqal)
RedChiefSouth:AddAirwing(AirwingMezzeh)

RedChiefSouth:AddBrigade(RedBrigadeSouth)
RedChiefSouth:AddBrigade(RedTroopsBrigadeSouth)

local RedSouthResourceEmpty, RedSouthResourceInfantry = RedChiefSouth:CreateResource(AUFTRAG.Type.ONGUARD, 2, 2, GROUP.Attribute.GROUND_INFANTRY)
RedChiefSouth:AddTransportToResource(RedSouthResourceInfantry, 1, 2, { GROUP.Attribute.AIR_TRANSPORTHELO })

-- chief sends CAS only when zones are lost, here we send constantly armor and arty
local RedResourceOccupied, RedResourceCapturePlatoon = RedChiefSouth:CreateResource(AUFTRAG.Type.ARTY, 1, 2)

RedChiefSouth:SetResponseOnTarget(1, 1, 1, TARGET.Category.GROUND, AUFTRAG.Type.BAI, 2, nil, CHIEF.Strategy.OFFENSIVE)
RedChiefSouth:SetResponseOnTarget(0, 0, 0, TARGET.Category.AIRCRAFT)

RedChiefSouth:AddStrategicZone(ZoneAlfa_Ops, 100, 1, RedResourceOccupied, RedSouthResourceEmpty)
RedChiefSouth:AddStrategicZone(ZoneBeta_Ops, 100, 2, RedResourceOccupied, RedSouthResourceEmpty)
RedChiefSouth:AddStrategicZone(ZoneCharlie_Ops, 100, 3, RedResourceOccupied, RedSouthResourceEmpty)
RedChiefSouth:AddStrategicZone(ZoneRayak_Ops, 100, 4, RedResourceOccupied, RedSouthResourceEmpty)
RedChiefSouth:AddStrategicZone(ZoneDelta_Ops, 100, 5, RedResourceOccupied, RedSouthResourceEmpty)

local IsRedChiefSouthAggressiveOrOffensive = math.random(0, 100)
RedChiefSouth:SetStrategy(CHIEF.Strategy.OFFENSIVE)

-- start delay to wait for opszones states, especially when reloading mission with DSM
TIMER:New(function()
    RedChiefSouth:Start()
end) :Start(120)

function RedChiefSouth:OnAfterZoneCaptured(From, Event, To, OpsZone)
    local CaptureMessage = string.format("%s captured %s", OpsZone:GetOwnerName(), OpsZone:GetName())
    trigger.action.outTextForCoalition(coalition.side.RED, CaptureMessage, 15)
    trigger.action.outSoundForCoalition(coalition.side.RED, "walkietalkie.ogg")
    env.info("OPL RedChiefSouth:OnAfterZoneCaptured -> " .. CaptureMessage)

    RedBrigadeSouth:SetSpawnZone(GetRedChiefSouthFrontlineSpawnZone(), UTILS.NMToMeters(100))

    local RandomCoordinate = OpsZone:GetRandomCoordinate()
    GroupsInZoneGoToCoordinate(OpsZone, RandomCoordinate)
    local ArmouredGuardMission = AUFTRAG:NewARMOREDGUARD(RandomCoordinate)
    RedChiefSouth:AddMission(ArmouredGuardMission)

    SpawnRedCamp(OpsZone, RandomCoordinate)

    local AnotherRandomCoordinate = OpsZone:GetRandomCoordinate()
    SpawnRedAAA(OpsZone, AnotherRandomCoordinate)
end

function RedChiefSouth:OnAfterZoneAttacked(From, Event, To, OpsZone)
    local AttackMessage = string.format("%s under attack", OpsZone:GetName())
    trigger.action.outTextForCoalition(coalition.side.RED, AttackMessage, 15)
    trigger.action.outSoundForCoalition(coalition.side.RED, "walkietalkie.ogg")

    env.info("OPL RedChiefSouth:OnAfterZoneAttacked -> " .. AttackMessage)
end

function RedChiefSouth:OnAfterZoneLost(From, Event, To, OpsZone)
    local LostMessage = string.format("%s captured by enemy", OpsZone:GetName())
    trigger.action.outTextForCoalition(coalition.side.RED, LostMessage, 15)
    trigger.action.outSoundForCoalition(coalition.side.RED, "walkietalkie.ogg")
    env.info("OPL RedChiefSouth:OnAfterZoneLost -> " .. OpsZone:GetName())

    if OpsZone:GetPreviousOwner() == coalition.side.RED then
        RedBrigadeSouth:SetSpawnZone(GetRedChiefSouthPreviousSpawnZone(), UTILS.NMToMeters(100))
    end
end

function RedChiefSouth:OnAfterZoneEmpty(From, Event, To, OpsZone)
    local EmptyMessage = string.format("Zone %s is empty.", OpsZone:GetName())
    trigger.action.outTextForCoalition(coalition.side.RED, EmptyMessage, 15)
    trigger.action.outSoundForCoalition(coalition.side.RED, "walkietalkie.ogg")
    env.info("OPL RedChiefSouth:OnAfterZoneEmpty -> " .. EmptyMessage)

    local RandomCoordinate = OpsZone:GetRandomCoordinate()
    local ArmouredGuardMission = AUFTRAG:NewARMOREDGUARD(RandomCoordinate)
    RedChiefSouth:AddMission(ArmouredGuardMission)

    local ZoneBase = OpsZone:GetZone()
    ZoneBase:RemoveJunk()
end

function RedChiefSouth:OnAfterMissionAssign(From, Event, To, Mission, Legions)
    if Mission:GetType() == AUFTRAG.Type.BAI or Mission:GetType() == AUFTRAG.Type.CAS or Mission:GetType() == AUFTRAG.Type.CASENHANCED then
        local EscortMission = AUFTRAG:NewESCORT(GroupToEscort, { x = 500, y = 3000, z = 500 }, 25, { "Air" })
        RedChiefNorth:AddMission(EscortMission)
    end
end

function RedChiefSouth:OnAfterStrategyChange(From, Event, To, Strategy)
    env.info("OPL RedChiefSouth " .. Strategy .. ", DEFCON " .. RedChiefSouth:GetDefcon())
end

function RedChiefSouth:OnAfterNewContact(From, Event, To, Contact)
    -- Gather info of contact.
    local ContactName = RedChiefSouth:GetContactName(Contact)
    local ContactType = RedChiefSouth:GetContactTypeName(Contact)
    local ContactThreat = RedChiefSouth:GetContactThreatlevel(Contact)
    local ContactGroup = RedChiefSouth:GetContactGroup(Contact)
    if ContactGroup:IsGround() or ContactGroup:IsShip() then
        local ContactCoordinate = ContactGroup:GetCoord()
        local MapMarker = MARKER:New(ContactCoordinate, ContactType):ReadOnly():ToRed()
        MapContactMarks[MapMarker] = ContactGroup
    end
    -- see attributes on GROUP.Attribute
    local ContactAttribute = ContactGroup:GetAttribute()

    local text = string.format("OPL RedChiefSouth detected NEW contact: Name=%s, Type=%s, Threat Level=%d, Attribute=%s", ContactName, ContactType, ContactThreat, ContactAttribute)
    env.info(text)
end

-- RED CHIEF North
RedChiefNorth = CHIEF:New(coalition.side.RED, RedDetection, "Red Chief North")
--SetDetectionTypes(DetectVisual, DetectOptical, DetectRadar, DetectIRST, DetectRWR, DetectDLINK)
RedChiefNorth:SetDetectionTypes(true, true, false, true, true, false)
RedChiefNorth:SetDetectStatics(false) -- not sure if enable or not (fry meme)
RedChiefNorth:SetClusterAnalysis(true, false, false)
RedChiefNorth:SetClusterRadius(20)
RedChiefNorth:SetBorderZones(ZoneRedBorderSet)
RedChiefNorth:AddConflictZone(ZONE_POLYGON:FindByName("Zone Bad Hollow"))
RedChiefNorth:AddRejectZone(ZoneBlueBorder)
-- chief does not detect Group.Category.HELICOPTER <--- UNCOMMENT BELOW
RedChiefNorth:SetFilterCategory(Group.Category.AIRPLANE, Group.Category.GROUND, Group.Category.SHIP)

-- a2a
RedChiefNorth:SetLimitMission(0, AUFTRAG.Type.CAP)
RedChiefNorth:SetLimitMission(0, AUFTRAG.Type.GCICAP)
RedChiefNorth:SetLimitMission(1, AUFTRAG.Type.ESCORT)
RedChiefNorth:SetLimitMission(1, AUFTRAG.Type.INTERCEPT)
-- a2g
RedChiefNorth:SetLimitMission(2, AUFTRAG.Type.BAI)
RedChiefNorth:SetLimitMission(2, AUFTRAG.Type.BOMBCARPET)
RedChiefNorth:SetLimitMission(2, AUFTRAG.Type.BOMBING)
RedChiefNorth:SetLimitMission(2, AUFTRAG.Type.CAS)
RedChiefNorth:SetLimitMission(2, AUFTRAG.Type.CASENHANCED)
RedChiefNorth:SetLimitMission(2, AUFTRAG.Type.GROUNDATTACK)
RedChiefNorth:SetLimitMission(2, AUFTRAG.Type.SEAD)
RedChiefNorth:SetLimitMission(2, AUFTRAG.Type.STRIKE)
-- ground
RedChiefNorth:SetLimitMission(4, AUFTRAG.Type.ARTY)
RedChiefNorth:SetLimitMission(20, AUFTRAG.Type.ARMOREDGUARD)
RedChiefNorth:SetLimitMission(2, AUFTRAG.Type.GROUNDESCORT)
RedChiefNorth:SetLimitMission(20, AUFTRAG.Type.NOTHING)
RedChiefNorth:SetLimitMission(20, AUFTRAG.Type.ONGUARD)
RedChiefNorth:SetLimitMission(20, AUFTRAG.Type.OPSTRANSPORT)
RedChiefNorth:SetLimitMission(20, AUFTRAG.Type.TROOPTRANSPORT)
RedChiefNorth:SetLimitMission(4, AUFTRAG.Type.GROUNDATTACK)
RedChiefNorth:SetLimitMission(8, AUFTRAG.Type.PATROLZONE)
RedChiefNorth:SetLimitMission(16, AUFTRAG.Type.CAPTUREZONE)

RedChiefNorth:AddAirwing(AirwingJirah)
RedChiefNorth:AddAirwing(AirwingTaftanaz)

RedChiefNorth:AddBrigade(RedBrigadeNorth)
RedChiefNorth:AddBrigade(RedTroopsBrigadeNorth)

local RedNorthResourceEmpty, RedNorthResourceInfantry = RedChiefNorth:CreateResource(AUFTRAG.Type.ONGUARD, 2, 2, GROUP.Attribute.GROUND_INFANTRY)
RedChiefNorth:AddTransportToResource(RedNorthResourceInfantry, 1, 2, { GROUP.Attribute.AIR_TRANSPORTHELO })

local RedNorthResourceOccupied, RedNorthResourceCapturePlatoon = RedChiefNorth:CreateResource(AUFTRAG.Type.ARTY, 1, 2)

RedChiefNorth:SetResponseOnTarget(1, 1, 1, TARGET.Category.GROUND, AUFTRAG.Type.BAI, 2, nil, CHIEF.Strategy.OFFENSIVE)
RedChiefNorth:SetResponseOnTarget(0, 0, 0, TARGET.Category.AIRCRAFT)

RedChiefNorth:AddStrategicZone(ZoneTaftanaz_Ops, 100, 1, RedNorthResourceOccupied, RedNorthResourceEmpty)
RedChiefNorth:AddStrategicZone(ZoneWhiskey_Ops, 100, 2, RedNorthResourceOccupied, RedNorthResourceEmpty)
RedChiefNorth:AddStrategicZone(ZoneXRay_Ops, 100, 3, RedNorthResourceOccupied, RedNorthResourceEmpty)
RedChiefNorth:AddStrategicZone(ZoneYankee_Ops, 100, 4, RedNorthResourceOccupied, RedNorthResourceEmpty)
RedChiefNorth:AddStrategicZone(ZoneZulu_Ops, 100, 5, RedNorthResourceOccupied, RedNorthResourceEmpty)

local IsRedChiefNorthAggressiveOrOffensive = math.random(0, 100)
RedChiefNorth:SetStrategy(CHIEF.Strategy.OFFENSIVE)

TIMER:New(function()
    RedChiefNorth:Start()
end) :Start(120)

function RedChiefNorth:OnAfterZoneCaptured(From, Event, To, OpsZone)
    local CaptureMessage = string.format("%s captured %s", OpsZone:GetOwnerName(), OpsZone:GetName())
    trigger.action.outTextForCoalition(coalition.side.RED, CaptureMessage, 15)
    trigger.action.outSoundForCoalition(coalition.side.RED, "walkietalkie.ogg")
    env.info("OPL RedChiefNorth:OnAfterZoneCaptured -> " .. CaptureMessage)

    RedBrigadeNorth:SetSpawnZone(GetRedChiefNorthFrontlineSpawnZone(), UTILS.NMToMeters(100))

    local RandomCoordinate = OpsZone:GetRandomCoordinate()
    GroupsInZoneGoToCoordinate(OpsZone, RandomCoordinate)
    local ArmouredGuardMission = AUFTRAG:NewARMOREDGUARD(RandomCoordinate)
    RedChiefNorth:AddMission(ArmouredGuardMission)

    SpawnRedCamp(OpsZone, RandomCoordinate)

    local AnotherRandomCoordinate = OpsZone:GetRandomCoordinate()
    SpawnRedAAA(OpsZone, AnotherRandomCoordinate)
end

function RedChiefNorth:OnAfterZoneAttacked(From, Event, To, OpsZone)
    local AttackMessage = string.format("%s under attack", OpsZone:GetName())
    trigger.action.outTextForCoalition(coalition.side.RED, AttackMessage, 15)
    trigger.action.outSoundForCoalition(coalition.side.RED, "walkietalkie.ogg")

    env.info("OPL RedChiefNorth:OnAfterZoneAttacked -> " .. AttackMessage)
end

function RedChiefNorth:OnAfterZoneLost(From, Event, To, OpsZone)
    local LostMessage = string.format("%s captured by enemy", OpsZone:GetName())
    trigger.action.outTextForCoalition(coalition.side.RED, LostMessage, 15)
    trigger.action.outSoundForCoalition(coalition.side.RED, "walkietalkie.ogg")
    env.info("OPL RedChiefNorth:OnAfterZoneLost -> " .. OpsZone:GetName())

    if OpsZone:GetPreviousOwner() == coalition.side.RED then
        RedBrigadeNorth:SetSpawnZone(GetRedChiefNorthPreviousSpawnZone(), UTILS.NMToMeters(100))
    end
end

function RedChiefNorth:OnAfterZoneEmpty(From, Event, To, OpsZone)
    local EmptyMessage = string.format("Zone %s is empty.", OpsZone:GetName())
    trigger.action.outTextForCoalition(coalition.side.RED, EmptyMessage, 15)
    trigger.action.outSoundForCoalition(coalition.side.RED, "walkietalkie.ogg")
    env.info("OPL RedChiefNorth:OnAfterZoneEmpty -> " .. EmptyMessage)

    local RandomCoordinate = OpsZone:GetRandomCoordinate()
    local ArmouredGuardMission = AUFTRAG:NewARMOREDGUARD(RandomCoordinate)
    RedChiefNorth:AddMission(ArmouredGuardMission)

    local ZoneBase = OpsZone:GetZone()
    ZoneBase:RemoveJunk()
end

function RedChiefNorth:OnAfterMissionAssign(From, Event, To, Mission, Legions)
    if Mission:GetType() == AUFTRAG.Type.BAI or Mission:GetType() == AUFTRAG.Type.CAS or Mission:GetType() == AUFTRAG.Type.CASENHANCED then
        local EscortMission = AUFTRAG:NewESCORT(GroupToEscort, { x = 500, y = 3000, z = 500 }, 25, { "Air" })
        RedChiefNorth:AddMission(EscortMission)
    end
end

function RedChiefNorth:OnAfterNewContact(From, Event, To, Contact)
    -- Gather info of contact.
    local ContactName = RedChiefNorth:GetContactName(Contact)
    local ContactType = RedChiefNorth:GetContactTypeName(Contact)
    local ContactThreat = RedChiefNorth:GetContactThreatlevel(Contact)
    local ContactGroup = RedChiefNorth:GetContactGroup(Contact)
    if ContactGroup:IsGround() then
        local ContactCoordinate = ContactGroup:GetCoord()
        local MapMarker = MARKER:New(ContactCoordinate, ContactType):ReadOnly():ToRed()
        MapContactMarks[MapMarker] = ContactGroup
    end
    -- see attributes on GROUP.Attribute
    local ContactAttribute = ContactGroup:GetAttribute()

    local text = string.format("OPL RedChiefNorth detected NEW contact: Name=%s, Type=%s, Threat Level=%d, Attribute=%s", ContactName, ContactType, ContactThreat, ContactAttribute)
    env.info(text)
end

-- utils / misc you know it

function GroupsInZoneGoToCoordinate(OpsZone, Coordinate)
    local Zone = OpsZone:GetZone()
    Zone:Scan({ Object.Category.UNIT }, { Unit.Category.GROUND_UNIT })
    local GroupSetInZone = Zone:GetScannedSetGroup()
    GroupSetInZone:ForEachGroup(
            function(groupInZone)
                local DebugMessage = string.format(
                        "OPL GroupsInZoneGoToCoordinate routing %s, group coalition %d, zone coalition %d",
                        groupInZone:GetName(),
                        groupInZone:GetCoalition(),
                        OpsZone:GetOwner()
                )
                env.info(DebugMessage)
                if groupInZone:GetCoalition() == OpsZone:GetOwner() then
                    groupInZone:RouteGroundOnRoad(Coordinate)
                end
            end
    )
end

function SpawnBlueCamp(OpsZone, SpawnCoordinate)
    local BlueCamp = SPAWN
            :NewWithAlias("Blue Army Camp", "BlueArmyCamp" .. OpsZone:GetName())
            :InitLimit(0, 0)
            :InitPositionCoordinate(SpawnCoordinate)
            :InitRandomizePosition(true, 200, 50)
            :InitGroupHeading(0, 359, 180)
            :Spawn()
end

function SpawnRedCamp(OpsZone, SpawnCoordinate)
    local RedCamp = SPAWN
            :NewWithAlias("Red Army Camp", "RedArmyCamp" .. OpsZone:GetName())
            :InitLimit(0, 0)
            :InitPositionCoordinate(SpawnCoordinate)
            :InitRandomizePosition(true, 200, 50)
            :InitGroupHeading(0, 359, 180)
            :Spawn()
end

function SpawnRedAAA(OpsZone, SpawnCoordinate)
    local RedAAATemplates = { "Red AAA-1", "Red AAA-2", "Red AAA-3", "Red AAA-4" }
    local RedAAAUnit = SPAWN
            :NewWithAlias("Red AAA Template", "Red AAA " .. OpsZone:GetName())
            :InitRandomizeTemplate(RedAAATemplates)
            :InitLimit(0, 0)
            :InitPositionCoordinate(SpawnCoordinate)
            :InitRandomizePosition(true, 200, 50)
            :InitGroupHeading(0, 359, 180)
            :Spawn()
end

ZZZLoaderAssert["4-chiefs"] = true